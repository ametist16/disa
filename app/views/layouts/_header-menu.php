<?php
	use yii\widgets\Menu;
?>

<nav class="header-menu">
	<?= Menu::widget([
		'options' => [
			'class' => 'header-menu__list',
		],
		'items' => [
			[
				'label' => 'Окна',
				'url' => ['/production/category', 'cat' => 'okna-pvh'],
				// 'active' => $this->context->id === 'site',
			],
			[
				'label' => 'Двери',
				'url' => ['/production/category', 'cat' => 'doors-pvc'],
				// 'active' => $this->context->id === 'site',
			],
			[
				'label' => 'Балконы и лоджии',
				'url' => ['/production/category', 'cat' => 'balconies-and-loggias'],
				// 'active' => $this->context->id === 'site',
			],
			[
				'label' => 'Алюминиевые окна и двери',
				'url' => ['/production/category', 'cat' => 'aluminum-windows-and-doors'],
				// 'active' => $this->context->id === 'site',
			],
			[
				'label' => 'Витражи',
				'url' => ['/production/category', 'cat' => 'stained'],
				// 'active' => $this->context->id === 'site',
			]
		],
		'itemOptions'=>['class'=>'header-menu__item'],
		'activeCssClass'=>'is-active',
	]); ?>
</nav>