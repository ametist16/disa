<?php
namespace app\modules\articles\controllers;

use yii\easyii\components\CategoryController;

class AController extends CategoryController
{
    /** @var string  */
    public $categoryClass = 'app\modules\articles\models\Category';

    /** @var string  */
    public $moduleName = 'articles';
}