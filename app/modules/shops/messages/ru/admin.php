<?php
return [
    'Texts' => 'Адреса магазинов',
    'Create text' => 'Создать новый адрес',
    'Edit text' => 'Редактировать адрес',
    'Text created' => 'Текст успешно создан',
    'Text updated' => 'Текст обновлен',
    'Text deleted' => 'Текст удален',
    'Text' => 'Текст удален',
    'Address' => 'Адрес',
    'Coordinates' => 'Координаты на карте',
    'Centermap' => 'Центр карты',
    'Phone' => 'Телефон',
    'Worktime' => 'График работы',
    'Manager' => 'Менеджер',
];