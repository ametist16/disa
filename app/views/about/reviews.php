<?php
	use app\modules\pages\api\Page;
	use app\modules\reviews\api\News as Reviews;
	use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>
<section class="section">
	<div class="section__inner">
		<div class="container">
			<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>
			
			<div class="reviews">
				<div class="reviews__list">
					<? foreach ($reviews as $key => $review): ?>
						<div class="reviews__item">
							<div class="reviews__name"><?=$review->title;?></div>
							<div class="reviews__date"><?=$review->date;?></div>
							
							<? if($review->model->video): ?>
								<div class="reviews__video">
									<iframe width="100%" height="280" src="<?= $review->model->video; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
								</div>
							<? else: ?>

								<div class="reviews__text">
									<?=$review->text;?>
								</div>

								<? if($review->image): ?>
									<div class="reviews__origin">
										<div class="reviews__origin-icon"><img src="/f/i/icon-review-origin.png" alt=""></div>
										<a href="<?=$review->image;?>" class="easyii-box">Посмотреть оригинал отзыва</a>
									</div>
								<? endif; ?>

							<? endif; ?>
						</div>
					<? endforeach; ?>
				</div>
			</div>

			<!-- Пагинация -->
			<?= Reviews::pages() ?>
		</div>
	</div>
</section>