<div class="advantages advantages_cooperation">
	<div class="advantages__list">
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/cart.png" alt=""></div>
				<div class="advantages__title">Широкий ассортимент <br>продукции высокого качества</div>
			</div>
			
		</div>

		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/instrument.png" alt=""></div>
				<div class="advantages__title">Производство и монтаж <br>конструкций любой сложности</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/skidki.png" alt=""></div>
				<div class="advantages__title">Гибкая система скидок <br>и индивидуальные ценовые предложения</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/support.png" alt=""></div>
				<div class="advantages__title">Строгое соблюдение сроков изготовления изделий <br>и выполнения работ</div>
			</div>
		</div>
	</div>
</div>