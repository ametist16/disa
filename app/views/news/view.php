<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Breadcrumbs;
	$this->title = $item->seo('title', $item->model->title);
	$this->registerMetaTag(
		['name' => 'description',
		'content' => $item->seo('description', $item->model->title)
	]);

	$this->params['breadcrumbs'][] = ['label' => 'Новости', 'url' => ['/news']];
	$this->params['breadcrumbs'][] = ['label' => $item->title];
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
		<div class="container">
			<div class="stock-item">
				<div class="stock-item__inner">
					<h1 class="stock-item__title"><?= Page::title($item->seo('h1', $item->title)); ?></h1>
					<div class="stock-item__img-wrap">
						<img src="<?= $item->image; ?>" alt="">
					</div>
					<div class="stock-item__text section__text"><?= $item->text; ?></div>
				</div>
			</div>
		</div>
</section>

<? if($item->photos): ?>
	<section class="section">
		<div class="container">
			<!-- Дополнительные фото новости -->
			<?= $this->render('_slides', [
				'slides' => $item->photos
			]);?>
		</div>
	</section>
<? endif; ?>
