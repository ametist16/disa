<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\pages\api\Page;
use app\modules\articles\api\Article as Articles;

class ArticlesController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function setSeo($target, $crumbs) {
        $this->view->title = $target->seo('title', $target->model->title);
        $this->view->registerMetaTag(
            [
                'name' => 'description',
                'content' => $target->seo('description', $target->model->title)
            ]
        );

        if (count($crumbs) > 0) {
            foreach ($crumbs as $key => $item) {
                $this->view->params['breadcrumbs'][] = $item;
            }
        }
    }

    private function getItem($value)
    {
        $current = Articles::cat($value->category_id);
        $item = [
            'label' => $current->title,
            'url' => '/articles/' . $value->slug
        ];

        return $item;
    }

    private function getMenu($categories, $className)
    {
        $categoryItems = [];

        foreach ($categories as $key => $value) {
            $categoryItems[] = $this->getItem($value);
        }

        return  [
            'options' => [
                'class' =>  $className . '-menu__list'
            ],
            'items' => $categoryItems,
            'itemOptions'=>['class' => $className . '-menu__item'],
            'activeCssClass'=>'is-active',
        ];
    }

    public function actionIndex()
    {
        $cats = Articles::cats();
        return $this->redirect('/articles/' . current($cats)->slug);
    }

    public function actionCategory($cat)
    {
        $category = Articles::cat($cat);
        $categories = Articles::tree();
        $subMenu = $this->getMenu($categories, 'subcatalog');

        $this->setSeo($category, [
            ['label' => $category->title]
        ]);

        return $this->render('category', [
            'category' => $category,
            'subMenu' => $subMenu,
            'items' => $category->items(['pagination' => ['pageSize' => 10]])
        ]);
    }

    public function actionView($cat, $slug)
    {
        $item = Articles::get($slug);
        $category = Articles::cat($cat);

        if ($category && $category->id === $item->category_id) {
            $this->setSeo($item, [
                ['label' => $category->title, 'url' => ['articles/category', 'cat' => $category->slug]],
                ['label' => $item->title]
            ]);

            Articles::plugin();

            return $this->render('view', [
                'item' => $item,
                'category' => $category
            ]);
        } else {
            throw new \yii\web\NotFoundHttpException("Нет такой категории или статьи");
        }

    }
}