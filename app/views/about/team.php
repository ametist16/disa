<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<div class="container">
	<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>
	<div class="section__text section__text_left"><?=$page->text;?></div>
	

	<div class="team-slider">
		<div class="team-slider__header">Наши сотрудники</div>
		<div class="team-slider__slider">
			<? foreach ($workers as $key => $worker): ?>
				<div class="team-slider__item">
					<div class="team-slider__img-wrapper">
						<img style="width: 150px" src="<?=$worker->image;?>" alt="">
					</div>
					<div class="team-slider__content">
						<div class="team-slider__title"><?=$worker->title;?></div>
						<div class="team-slider__prof"><?=$worker->job;?></div>
						<div class="team-slider__text"><?=$worker->text;?></div>
					</div>
				</div>
			<? endforeach; ?>
		</div>
	</div>
	
</div>
