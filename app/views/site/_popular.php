<div class="popular">
	<div class="popular__inner">
		<div class="popular__tabs js__tab">
			<div class="popular__tab-head">
				<div class="popular__tab-head-item js__tab-btn is-active">2-х створчатые окна</div>
				<div class="popular__tab-head-item js__tab-btn">3-х створчатые окна</div>
				<div class="popular__tab-head-item js__tab-btn">Алюминевые окна</div>
			</div>
			<div class="popular__tab-list">
				<div class="popular__tab js__tab-item is-active">
					<div class="popular__list">
						<div class="popular__item">
							<div class="popular__title">BRUSBOX 60</div>
							<div class="popular__advantage">ЭКОНОМИЧНЫЕ И КАЧЕСТВЕННЫЕ</div>
							<div class="popular__img"><img src="/uploads/popular/window-1.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">1200 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">60 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">32 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">3</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,64 м2C/Вт</span>
								</div>
							</div>
							<div class="popular__caption">Элегантный внешний вид<br>Двойная защита от сквозняков</div>
							<div class="popular__price">от <span class="popular__price-val">7 500</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
						<div class="popular__item">
							<div class="popular__title">BRUSBOX 70 (5)</div>
							<div class="popular__advantage">светлые и тёплые</div>
							<div class="popular__img"><img src="/uploads/popular/window-2.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">1200 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">70 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">32/40 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">5</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,80 м2C/Вт</span>
								</div>
							</div>
							<div class="popular__caption">Больше солнечного света<br>Повышенная шумо- и теплоизоляция</div>
							<div class="popular__price">от <span class="popular__price-val">7 800</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
						<div class="popular__item">
							<div class="popular__title">BRUSBOX 70 (6)</div>
							<div class="popular__advantage">качество, проверенное годами</div>
							<div class="popular__img"><img src="/uploads/popular/window-3.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">1200 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">70 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">32/40 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">6</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,90 м2C/Вт</span>
								</div>
							</div>
							<div class="popular__caption">Эталон надежности и шумоизоляции<br>Абсолютный лидер теплосбережения</div>
							<div class="popular__price">от <span class="popular__price-val">8 350</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
					</div>
				</div>
				<div class="popular__tab js__tab-item">
					<div class="popular__list">
						<div class="popular__item">
							<div class="popular__title">BRUSBOX 60</div>
							<div class="popular__advantage">ЭКОНОМИЧНЫЕ И КРАСИВЫЕ</div>
							<div class="popular__img"><img src="/uploads/popular/window-3-a.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">2000 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">60 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">32 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">3</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,64 м2C/Вт</span>
								</div>
							</div>
							<div class="popular__caption">Элегантный внешний вид<br>Двойная защита от сквозняков</div>
							<div class="popular__price">от <span class="popular__price-val">10 900</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
						<div class="popular__item">
							<div class="popular__title">BRUSBOX 70 (5)</div>
							<div class="popular__advantage">качество, проверенное годами</div>
							<div class="popular__img"><img src="/uploads/popular/window-3-b.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">2000 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">70 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">32/40 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">5</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,80 м2C/Вт</span>
								</div>
							</div>
							<div class="popular__caption">Больше солнечного света<br>Повышенная шумо- и теплоизоляция</div>
							<div class="popular__price">от <span class="popular__price-val">11 400</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
						<div class="popular__item">
							<div class="popular__title">BRUSBOX 70 (6)</div>
							<div class="popular__advantage">светлые и тёплые</div>
							<div class="popular__img"><img src="/uploads/popular/window-3-c.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">2000 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">70 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">32/40 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">6</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,80 м2C/Вт</span>
								</div>
							</div>
							<div class="popular__caption">Эталон надежности<br>Идеальная шумоизоляция<br>Абсолютный лидер теплосбережения</div>
							<div class="popular__price">от <span class="popular__price-val">12 100</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
					</div>
				</div>
				<div class="popular__tab js__tab-item">
					<div class="popular__list">
						<div class="popular__item">
							<div class="popular__title">Alumark S44 </div>
							<div class="popular__advantage">холодный алюминий</div>
							<div class="popular__img"><img src="/uploads/popular/window-7.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">1200 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">44 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">24 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">1</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,64 м2C/Вт</span>
								</div>
							</div>
						<!--	<div class="popular__caption">Эталон надежности и шумоизоляции<br>Абсолютный лидер теплосбережения</div>-->
							<div class="popular__price">от <span class="popular__price-val">от 11700</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
						<div class="popular__item">
							<div class="popular__title">Alumark S54</div>
							<div class="popular__advantage">теплый алюминий</div>
							<div class="popular__img"><img src="/uploads/popular/window-8.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">1200 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">54 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">32 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">3</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,80 м2C/Вт</span>
								</div>
							</div>
							<!--<div class="popular__caption">Элегантный внешний вид<br>Двойная защита от сквозняков</div>-->
							<div class="popular__price">от <span class="popular__price-val">от 16500</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
						<div class="popular__item">
							<div class="popular__title">Alumark S70</div>
							<div class="popular__advantage">теплый алюминий</div>
							<div class="popular__img"><img src="/uploads/popular/window-9.jpg" alt=""></div>
							<div class="popular__props">
								<div class="popular__prop">
									<span class="popular__prop-name">Размер изделия:</span><span class="popular__prop-val">1200 х 1300</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина профиля:</span><span class="popular__prop-val">70 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Ширина стеклопакета:</span><span class="popular__prop-val">40 мм</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Воздушные камеры:</span><span class="popular__prop-val">3</span>
								</div>
								<div class="popular__prop">
									<span class="popular__prop-name">Сопротивл. теплоотдаче:</span><span class="popular__prop-val">0,90 м2C/Вт</span>
								</div>
							</div>
							<!--<div class="popular__caption">Больше солнечного света<br>Повышенная шумо- и теплоизоляция</div>-->
							<div class="popular__price">от <span class="popular__price-val">от 20600</span> руб.</div>
							<div class="popular__btn">
								<button href="#" class="btn" data-popup-open="metering">Заказать окно</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>