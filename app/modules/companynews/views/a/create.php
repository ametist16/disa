<?php
$this->title = Yii::t('easyii/companynews', 'Create news');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>