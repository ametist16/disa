$.CreateModule({
	name: 'Sliders',
	data: {},
	options: {
		sliderOptions: {

		}
	},
	hooks: {
		beforeCreate: function () {},
		create: function () {
			this._create();
		},
		bindEvent: function () {
			$(document).on('changetab', this._reinit.bind(this));
		},
		afterCreate: function () {}
	},
	privateMethods: {
		_create: function () {
			this._initSlider();
		},
		_initSlider:function () {
			if ($(this.element).is(':visible')) {
				$(this.element).slick(this.options.sliderOptions);
			}
		},
		_reinit: function () {
			// $(this.element).slick('reinit');
			if (!$(this.element).hasClass('slick-initialized')) {
				this._initSlider();
			}
		},
		_unBindEvent: function () {
			$(this.element).off(this.hash);
		}
	},
	publicMethods: {}
});

$(function () {
	$('.top-slider__slider').sliders({
		options: {
			sliderOptions: {
				arrows: true,
				dots: true,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							arrows: false
						}
					}
				]
			}
		}
	});

	$('.js__catalog-certificate').sliders({
		options: {
			sliderOptions: {
				arrows: false,
				dots: true,
				slidesToShow: 2,
				slidesToScroll: 2,
			}
		}
	});

	$('.works-slider__list').sliders({
		options: {
			sliderOptions: {
				arrows: true,
				dots: false,
				slidesToShow: 5,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							arrows: false,
							centerMode: false,
							variableWidth: false,
							dots: true
						}
					}
				]
			}
		}
	});

	$('.feedback-slider__list').sliders({
		options: {
			sliderOptions: {
				arrows: true,
				dots: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				adaptiveHeight: true,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 1,
							centerMode: true,
							arrows: false,
							dots: true
						}
					}
				]
			}
		}
	});

	$('.clients-slider__list').sliders({
		options: {
			sliderOptions: {
				arrows: true,
				dots: false,
				slidesToShow: 5,
				slidesToScroll: 1,
				rows: 2,
				slidesPerRow: 1,
				responsive: [
					{
						breakpoint: 1280,
						settings: {
							arrows: false,
							dots: true
						}
					},
					{
						breakpoint: 768,
						settings: {
							slidesToShow: 2,
						}
					}
				]
			}
		}
	});

	$('.team-slider__slider').sliders({
		options: {
			sliderOptions: {
				arrows: true,
				dots: false,
				slidesToShow: 2,
				slidesToScroll: 1,
				variableWidth: true
			}
		}
	});

	$('.production-slider__list').sliders({
		options: {
			sliderOptions: {
				arrows: true,
				dots: false,
				slidesToShow: 3,
				slidesToScroll: 1,
				responsive: [
					{
						breakpoint: 768,
						settings: {
							arrows: false,
							dots: true

						}
					}
				]
			}
		}
	});
});