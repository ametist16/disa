<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Menu;
	use yii\widgets\Breadcrumbs;

	$this->title = $category->seo('title', $category->model->title);
	$this->registerMetaTag(
		['name' => 'description',
		'content' => $category->seo('description', $category->model->title)
	]);

	$this->params['breadcrumbs'][] = ['label' => 'Каталог продукции', 'url' => ['/production']];
	if ($parent) {
		$this->params['breadcrumbs'][] = ['label' => $parent->model->title, 'url' => ['/production/category', 'cat' => $parent->model->slug]];
	}
	$this->params['breadcrumbs'][] = ['label' => $category->model->title];
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="container">
		
		<div class="page">
			<div class="page__inner">
				<div class="page__col page__col_left">
					<!-- Меню категорий -->
					<nav>
						<?= Menu::widget($categoryMenu); ?>
					</nav>
					

					<? if ($certificates && count($certificates) > 0): ?>
						<?= $this->render('_certificates', [
							'certificates' => $certificates
						]);?>
					<? endif; ?>

					<? if ($reviews && count($reviews) > 0): ?>
						<?= $this->render('_reviews', [
							'reviews' => $reviews
						]);?>
					<? endif; ?>

				</div>
				<div class="page__col page__col_right">
					<h1 class="stock-item__title stock-item__title_inner"><?= Page::title($category->seo('h1', $category->title)); ?></h1>


					
					<? //странички с исключительным контентом: ?>
					<? if ($categoryId === 'glass-unit'): ?>
						<?= $this->render('./pages/_' . $categoryId, [
								'category'=>$category->model
						]);?>
					<? else: ?>
					<? if ($subMenu): ?>
						<!-- Меню подкатегорий -->
						<nav>
							<?= Menu::widget($subMenu); ?>
						</nav>
					<? endif; ?>

					<div class="page__text section__text">
						<!-- Описание категории перед продуктами -->
						<?= $category->model->text; ?>
					</div>

					<? if ($items): ?>
						<!-- Элементы категории -->
						<ul>
							<? foreach($items as $item): ?>
								<?= $this->render('_item', [
									'item' => $item
								]);?>
							<? endforeach; ?>
						</ul>
					<? endif; ?>

					<div class="page__text section__text">
						<!-- Описание категории после продуктов -->
						<?= $category->model->text_2; ?>
					</div>
					<? endif; ?>
					<!-- Примеры работ внизу страницы -->
					<? if(count($works) > 0): ?>
						<div class="section__title section__title_small section__title_inner">
							<?= Page::title('примеры наших работ'); ?>
						</div>
						<?= $this->render('_slides', [
							'slides' => $works
						]);?>
					<? endif; ?>
				</div>

			</div>
		</div>
	</div>
</section>

<?= $this->render('_form', []);?>

