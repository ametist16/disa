<?php
	use yii\easyii\models\Setting;
?>

<header class="header">
	<div class="section">
		<div class="container">
			<div class="header-top">
				<div class="header-top__inner">
					<div class="header-top__menu-mob">
						<div class="header-top__menu-mob-line"></div>
					</div>
					<a class="header-top__logo" href="/">
						<img src="/f/i/top-logo.png" alt="">
					</a>

					<div class="header-top__btns">
						<button class="header-top__btn" data-popup-open="form" onclick="yaCounter32137425.reachGoal('click_header')">Заказать звонок</button>
						<button class="header-top__btn" data-popup-open="metering" onclick="yaCounter32137425.reachGoal('zamer_header')">Записаться на замер</button>
					</div>
					
					<!-- Адреса -->
					<div class="header-top__adress">
						<div class="header-top__adress-item">
							<span class="header-top__adress-area">Беж. р-н: </span>
							<span class="header-top__adress-location">ул. Харьковская 2</span>
						</div>
						<div class="header-top__adress-item">
							<span class="header-top__adress-area">Совет. р-н: </span>
							<span class="header-top__adress-location">ул. Ромашина 1а</span>
						</div>
					</div>
					
					<!-- Телефоны -->
					<div class="header-top__phones">
						<a href="tel:+74832305757" class="header-top__phone">
							<span class="header-top__phone-code">+7(4832)</span>
							<span class="header-top__phone-num">30-57-57</span>
						</a>
						<a href="tel:+74832642222" class="header-top__phone">
							<span class="header-top__phone-code">+7(4832)</span>
							<span class="header-top__phone-num">64-22-22</span>
						</a>
					</div>

				</div>
			</div>
		</div>
	</div>

	<!-- Муеню каталога -->
	<div class="section section_orange">
		<div class="container">
			<?= $this->render('_header-menu', []);?>
		</div>
	</div>
	
	<!-- Главное меню -->
	<div class="section">
		<div class="container">
			<?= $this->render('_main-menu', []);?>
		</div>
	</div>
	<div class="btn-to-top"><img src="/f/i/btn-to-top.png" alt=""></div>
</header>
