<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\pages\api\Page;

class CooperationController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionDealership()
    {
        $page = Page::get('page-dealership');

        return $this->render('dealership', [
            'page' => $page
        ]);
    }

    public function actionCorporateClient()
    {
        $page = Page::get('page-corporate-client');

        return $this->render('corporate-client', [
            'page' => $page
        ]);
    }
}