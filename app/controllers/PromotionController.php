<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\shares\api\Shares;

class PromotionController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    private function getItem($value)
    {
        $current = Shares::cat($value->category_id);

        $item = [
            'label' => $current->title,
            'url' => '/' . $this->id . '/' . $value->slug
        ];

        if ($current->children) {
            $items['items'] = [];

            foreach ($current->children as $key => $children) {
                $item['items'][] = $this->getItem($children);
            }
        }

        return $item;
    }

    private function getMenu($categories, $className)
    {
        $categoryItems = [];

        foreach ($categories as $key => $value) {
            $categoryItems[] = $this->getItem($value);
        }

        return  [
            'options' => [
                'class' =>  $className . '-menu__list'
            ],
            'items' => $categoryItems,
            'itemOptions'=>['class' => $className . '-menu__item'],
            'activeCssClass'=>'is-active',
        ];
    }

    public function actionIndex()
    {
        return $this->redirect('/promotion/current');
    }

    public function setSeo($target, $crumbs) {
        $this->view->title = $target->seo('title', $target->model->title);
        $this->view->registerMetaTag(
            [
                'name' => 'description',
                'content' => $target->seo('description', $target->model->title)
            ]
        );

        if (count($crumbs) > 0) {
            foreach ($crumbs as $key => $item) {
                $this->view->params['breadcrumbs'][] = $item;
            }
        }
    }

    public function actionCategory($cat)
    {
        $category = Shares::cat($cat);
        $categories = Shares::tree();
        $categoryMenu = $this->getMenu($categories, 'tags');

        $this->setSeo($category, [
            ['label' => 'Акции'],
            ['label' => $category->model->title]
        ]);

        return $this->render('category', [
            'categoryMenu' => $categoryMenu,
            'category' => $category,
            'items' => $category->items(['pagination' => ['pageSize' => 10]])
        ]);
    }

    public function actionView($cat, $slug)
    {
        $category = Shares::cat($cat);
        $item = Shares::get($slug);

        $this->setSeo($item, [
            ['label' => 'Акции'],
            ['label' => $category->model->title, 'url' => ['promotion/category', 'cat' => $category->slug]],
            ['label' => $item->model->title]
        ]);

        return $this->render('view', [
            'item' => $item,
            'category' => $category
        ]);
    }
}