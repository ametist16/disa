<? if ($items): ?>
	<!-- Элементы категории -->
	<ul>
        <? foreach ($items as $item): ?>
            <?= $this->render('_item', [
                'item' => $item
            ]); ?>
        <? endforeach; ?>
	</ul>
<? endif; ?>


<div class="page__text section__text">
    <?= $category->text; ?>
</div>

<div class="catalog-inner">
	<div class="catalog-inner__list">
		<a href="/production/glass-unit/sumozasitnye" class="catalog-inner__item">
			<div class="catalog-inner__img-wrapper">
				<img src="/f/i/steklopaketi/sumozasitnye.jpg" alt="" class="catalog-inner__img">
			</div>
			<div class="catalog-inner__title">Шумозащитные</div>
			<div class="catalog-inner__description">
				 высокий уровень звукоизоляции<br>
				 защита от вибраций
			</div>
		</a>
		
		<a href="/production/glass-unit/energosberegausie" class="catalog-inner__item">
			<div class="catalog-inner__img-wrapper">
				<img src="/f/i/steklopaketi/energosberegausie.jpg" alt="" class="catalog-inner__img">
			</div>
			<div class="catalog-inner__title">Энергосберегающие</div>
			<div class="catalog-inner__description">
				снижение потерь тепла до 70%<br>
				защита от жары
			</div>
		</a>
		<a href="/production/glass-unit/multifunkcionalnye" class="catalog-inner__item">
			<div class="catalog-inner__img-wrapper">
				<img src="/f/i/steklopaketi/solnceotrazausie.jpg" alt="" class="catalog-inner__img">
			</div>
			<div class="catalog-inner__title">Мультифункциональные</div>
			<div class="catalog-inner__description">сохранение тепла<br>
				преграда для жары и УФ-лучей
			</div>
		</a>
		<a href="/production/glass-unit/tonirovannye" class="catalog-inner__item">
			<div class="catalog-inner__img-wrapper">
				<img src="/f/i/steklopaketi/tonirovannye.jpg" alt="" class="catalog-inner__img">
			</div>
			<div class="catalog-inner__title">Тонированные</div>
			<div class="catalog-inner__description">
				солнцезащита<br>
				обеспечение приватности
			</div>
		</a>
		<a href="/production/glass-unit/s-tripleksom" class="catalog-inner__item">
			<div class="catalog-inner__img-wrapper">
				<img src="/f/i/steklopaketi/udarostojkie.jpg" alt="" class="catalog-inner__img">
			</div>
			<div class="catalog-inner__title">Триплекс</div>
			<div class="catalog-inner__description">
			    выдерживает высокие ударные<br>
				нагрузки; огнестойкий
			</div>
		</a>
		
		<a href="/production/glass-unit/s-dekorativnoj-raskladkoj" class="catalog-inner__item">
			<div class="catalog-inner__img-wrapper">
				<img src="/f/i/steklopaketi/dekorativnye-sprosy.jpg" alt="" class="catalog-inner__img">
			</div>
			<div class="catalog-inner__title">Декоративная раскладка</div>
			<div class="catalog-inner__description">
			    повышает эстетичность<br>
				придает индивидуальности
			</div>
		</a>
	</div>
</div>

<div class="page__text section__text">
    <?= $category->text_2; ?>
</div>

