<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
    use yii\easyii\widgets\Redactor;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['class' => 'model-form']
]); ?>


<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'city') ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'district') ?>
	</div>
	<div class="col-sm-12">
		<?= $form->field($model, 'address') ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'coordinates') ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'centermap') ?>
	</div>
</div>

<hr>

<div class="row">
	<div class="col-sm-6">
		<?= $form->field($model, 'manager')->widget(Redactor::className(),[
		    'options' => [
		        'minHeight' => 100,
		        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'services']),
		        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'services']),
		        'plugins' => ['fullscreen']
		    ]
		]) ?>
	</div>
	<div class="col-sm-6">
		<?= $form->field($model, 'phone')->widget(Redactor::className(),[
		    'options' => [
		        'minHeight' => 100,
		        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'services']),
		        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'services']),
		        'plugins' => ['fullscreen']
		    ]
		]) ?>
	</div>
</div>
<div class="row">
	<div class="col-sm-12">
		<?= $form->field($model, 'worktime')->widget(Redactor::className(),[
		    'options' => [
		        'minHeight' => 200,
		        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'services']),
		        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'services']),
		        'plugins' => ['fullscreen']
		    ]
		]) ?>
	</div>
	
</div>

<?//= (IS_ROOT) ? $form->field($model, 'slug') : '' ?>
<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>