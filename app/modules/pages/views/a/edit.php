<?php
$this->title = Yii::t('easyii/pages', 'Edit page');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>