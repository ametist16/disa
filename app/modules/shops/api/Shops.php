<?php
namespace app\modules\shops\api;

use Yii;
use yii\easyii\components\API;
use yii\easyii\helpers\Data;
use yii\helpers\Url;
use app\modules\shops\models\Text as TextModel;
use yii\helpers\Html;

/**
 * Text module API
 * @package app\modules\shops\api
 *
 * @method static get(mixed $id_slug) Get text block by id or slug
 */
class Shops extends API
{
    private $_shops = [];

    public function init()
    {
        parent::init();

        $this->_shops = Data::cache(TextModel::CACHE_KEY, 3600, function(){
            return TextModel::find()->asArray()->all();
        });
    }

    public function api_all()
    {
        usort($this->_shops, function ($a, $b) {
            if ($a["text_id"] == $b["text_id"]) {
                return 0;
            }
            return ($a["text_id"] < $b["text_id"]) ? -1 : 1;
        });

        return $this->_shops;
    }

    public function api_get($id_slug)
    {
        if(($text = $this->findText($id_slug)) === null){
            return $this->notFound($id_slug);
        }
        return LIVE_EDIT ? API::liveEdit($text['text'], Url::to(['/admin/shops/a/edit/', 'id' => $text['text_id']])) : $text['shops'];
    }

    private function findText($id_slug)
    {
        foreach ($this->_shops as $item) {
            if($item['slug'] == $id_slug || $item['text_id'] == $id_slug){
                return $item;
            }
        }
        return null;
    }

    private function notFound($id_slug)
    {
        $text = '';

        if(!Yii::$app->user->isGuest && preg_match(TextModel::$SLUG_PATTERN, $id_slug)){
            $text = Html::a(Yii::t('easyii/shops/api', 'Create text'), ['/admin/shops/a/create', 'slug' => $id_slug], ['target' => '_blank']);
        }

        return $text;
    }
}