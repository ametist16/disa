<?php
namespace app\assets;

class AppAsset extends \yii\web\AssetBundle
{
    public $sourcePath = '@app/media';
    public $css = [
        'css/slick.css',
        'css/slick-theme.css',
        'css/styles.css',
    ];
    public $js = [
        'js/jquery.modulecreator.min.js',
        'js/slick.min.js',
        'js/app.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];
}
