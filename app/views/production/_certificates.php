<div class="aside-block">
	<div class="aside-block__inner">
		<div class="aside-block__title">Наши сертификаты</div>
		<div class="aside-block__content">
			<div class="catalog-certificate">
				<div class="catalog-certificate__list js__catalog-certificate">
					<? foreach($certificates as $item): ?>
						<div class="catalog-certificate__item">
							<a class="easyii-box" href="<?= $item->image; ?>">
								<img src="<?= $item->thumb(210); ?>" alt="">
							</a>
						</div>
					<? endforeach; ?>
				</div>
			</div>
		</div>
	</div>
</div>