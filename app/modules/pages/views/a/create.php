<?php
$this->title = Yii::t('easyii/pages', 'Create page');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>