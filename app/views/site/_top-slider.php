

<section class="section">
	<div class="top-slider">
		<div class="top-slider__inner">
			<div class="top-slider__slider">

				<? foreach ($slides as $key => $slide): ?>
					<div
						class="top-slider__item"
						style="background-image: url('<?= $slide->image; ?>');"
					>
						<div class="container">
							<div class="top-slider__body">
								<? if ($slide->title): ?>
									<div class="top-slider__btn top-slider__btn_<?= $slide->model->btn_position; ?>">
										<a href="<?= $slide->link; ?>" class="btn"><?= $slide->title; ?></a>
									</div>
								<? endif; ?>
							</div>
						</div>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</div>

	<a href="<?= $slide->link; ?>" class="top-slider top-slider_phone">
		<div class="top-slider__inner">
			<div class="top-slider__slider">

				<? foreach ($slides as $key => $slide): ?>
					<div
						class="top-slider__item"
						style="background-image: url('<?= $slide->image; ?>');"
					>
						<div class="container">
							<div class="top-slider__body">

							</div>
						</div>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</a>
</section>