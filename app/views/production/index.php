<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Menu;
	use yii\helpers\Url;
	use yii\widgets\Breadcrumbs;

	$this->title = $page->seo('title', $page->model->title);
	$this->registerMetaTag(
		['name' => 'description',
		'content' => $page->seo('description', $page->model->title)
	]);

	$this->params['breadcrumbs'][] = ['label' => 'Каталог продукции'];
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="container">
		<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>
		<div class="section__text">В нашей компании вы можете купить окна из  ПВХ и алюминия уже сегодня! Мы располагаем собственным производством и предоставляем клиентам огромный ассортимент светопрозрачных конструкций из алюминиевого и ПВХ профилей. Всегда предложим вам лучшее решение, идеально подобранное под ваше помещение: частный дом, квартиру, офис, магазин или салон красоты. Познакомиться с ценами на пластиковые окна в Брянске вы можете непосредственно на этом официальном сайте.</div>

		<div class="catalog">
			<div class="catalog__list">
				<? foreach ($categoryList as $key => $category): ?>
					<a href="<?= Url::toRoute(['production/category', 'cat' => $category->slug]); ?>" class="catalog__item" style="background-image: url('<?= $category->image ?>');">
						<div class="catalog__body">
							<div class="catalog__icon">
								<img src="<?= $category->icon ?>" alt="">
							</div>
							<div class="catalog__title"><?= $category->title ?></div>
						</div>
					</a>
				<? endforeach; ?>
			</div>
		</div>

	</div>
</section>
