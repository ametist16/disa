<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\pages\api\Page;
use app\modules\workers\api\News as Workers;
use app\modules\reviews\api\News as Reviews;
use app\modules\certificate\api\Certificate;

class AboutController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function setSeo($target, $crumbs) {
        $this->view->title = $target->seo('title', $target->model->title);
        $this->view->registerMetaTag(
            [
                'name' => 'description',
                'content' => $target->seo('description', $target->model->title)
            ]
        );

        if (count($crumbs) > 0) {
            foreach ($crumbs as $key => $item) {
                $this->view->params['breadcrumbs'][] = $item;
            }
        }
    }

    public function actionIndex()
    {
        return $this->redirect('/about/about-us');
    }

    public function actionAboutUs()
    {
        $page = Page::get('page-about-us');

        $this->setSeo($page, [
            ['label' => 'О компании'],
            ['label' => $page->title]
        ]);

        return $this->render('about-us', [
            'page' => $page
        ]);
    }

    public function actionCareer()
    {
        $page = Page::get('page-career');

        $this->setSeo($page, [
            ['label' => 'О компании'],
            ['label' => $page->title]
        ]);

        return $this->render('career', [
            'page' => $page
        ]);
    }

    public function actionCertificate()
    {
        $page = Page::get('page-certificate');
        $certificates = Certificate::cat('certificate');

        Certificate::plugin();

        $this->setSeo($page, [
            ['label' => 'О компании'],
            ['label' => $page->title]
        ]);

        return $this->render('certificate', [
            'certificates' => $certificates->photos(['pagination' => ['pageSize' => 999]]),
            'page' => $page
        ]);
    }

    public function actionTeam()
    {
        $page = Page::get('page-team');
        $workers = Workers::items();

        $this->setSeo($page, [
            ['label' => 'О компании'],
            ['label' => $page->title]
        ]);

        return $this->render('team', [
            'page' => $page,
            'workers' => $workers
        ]);
    }

    public function actionReviews()
    {
        $page = Page::get('page-reviews');
        $reviews = Reviews::items(['pagination' => ['pageSize' => 10]]);

        Reviews::plugin();

        $this->setSeo($page, [
            ['label' => 'О компании'],
            ['label' => $page->title]
        ]);

        return $this->render('reviews', [
            'page' => $page,
            'reviews' => $reviews
        ]);
    }
}