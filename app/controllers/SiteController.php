<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\pages\api\Page;
use app\modules\topslider\api\Carousel;
use app\modules\productions\api\Catalog;
use app\modules\services\api\Services;
use app\modules\works\api\Gallery;
use app\modules\reviews\api\News as Reviews;

class SiteController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    private function getCategoryList ()
    {
        $catalogCategories = Catalog::cats();

        return array_filter($catalogCategories, function ($item, $index) {
            return ($item->depth === '0');
        }, ARRAY_FILTER_USE_BOTH);
    }

    public function actionIndex()
    {
        $page = Page::get('page-index');
        $categoryList = $this->getCategoryList();
        $slides = Carousel::items();
        $services = Services::items();
        $reviews = Reviews::items([
            'pagination' => ['pageSize' => 999]
        ]);

//        $reviews = array_filter($reviews, function ($item) {
//            return $item->model['video'];
//        });


        $categories = Gallery::tree();

        $categoryWorks = [];

        foreach ($categories as $key => $value) {

            $category = Gallery::cat($value->slug);

            $categoryWorks[] = [
                'label' => $value->title,
                'url' => $value->slug,
                'items' => $category->photos()
            ];
        }

        Gallery::plugin();

        return $this->render('index', [
            'page' => $page,
            'categoryList' => $categoryList,
            'slides' => $slides,
            'services' => $services,
            'reviews' => $reviews,
            'works' => Gallery::last(10),
            'categoryWorks' => $categoryWorks
        ]);
    }
}