<?php
namespace app\modules\topslider\api;

class CarouselObject extends \yii\easyii\components\ApiObject
{
    public $image;
    public $link;
    public $title;
    public $text;
}