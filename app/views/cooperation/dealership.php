<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Breadcrumbs;

	$this->title = $page->seo('title', $page->model->title);
	$this->registerMetaTag(
		['name' => 'description',
		'content' => $page->seo('description', $page->model->title)
	]);

	$this->params['breadcrumbs'][] = ['label' => $page->title];
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="section__inner">
		<div class="container">
			<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>

			<?= $this->render('_cooperation-menu', []);?>

			<div class="section__subtitle">
				<?=$page->text;?>
			</div>

			<div class="section__text section__text_left">
				<?=$page->text_2;?>
			</div>
			
			<?= $this->render('_advantages-dealership', []);?>
		</div>
	</div>
	
</section>

<?= $this->render('_cooperation-form', []);?>