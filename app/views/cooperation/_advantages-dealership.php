<div class="advantages advantages_cooperation">
	<div class="advantages__list">
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/cart.png" alt=""></div>
				<div class="advantages__title">Широкий ассортимент <br>выпускаемой продукции</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/people.png" alt=""></div>
				<div class="advantages__title">Индивидуальный подход <br>к каждому дилеру</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/instrument.png" alt=""></div>
				<div class="advantages__title">Все необходимые комплектующие <br>для проведения монтажных работ</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/skidki.png" alt=""></div>
				<div class="advantages__title">Гибкую систему скидок <br>для новичков и постоянных клиентов</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/phone.png" alt=""></div>
				<div class="advantages__title">Предварительный просчет <br>стоимости заказа по телефону</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/payment.png" alt=""></div>
				<div class="advantages__title">Специальные условия <br>оплаты</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/delivery.png" alt=""></div>
				<div class="advantages__title">Квалифицированную доставку <br>готовых конструкций <br>и комплектующих для монтажа</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/support.png" alt=""></div>
				<div class="advantages__title">Информационную <br>и техническую поддержку <br>клиентов в режиме on-line</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/eye.png" alt=""></div>
				<div class="advantages__title">Бесплатные образцы <br>продукции, выполненные <br>по заказу клиента</div>
			</div>
			
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/docs.png" alt=""></div>
				<div class="advantages__title">Полный пакет документов</div>
			</div>
			
		</div>
	</div>
</div>