<?php
	use yii\widgets\Menu;
	use app\modules\productions\api\Catalog;
	use app\modules\services\api\Services;

	$categories = Catalog::tree();
	$categoryMenu = Catalog::menu($categories, 'main', $this->context->actionParams['cat']);

	$servicesMenu = Services::menu('services', $this->context->actionParams['slug']);
?>

<nav class="main-menu">
	<?= Menu::widget([
		'options' => [
			'class' => 'main-menu__list'
		],
		'items' => [
			[
				'label' => 'Главная',
				'url' => ['/'],
				'active' => $this->context->id === 'site',
			],
			[
				'label' => 'О компании',
				'active' => $this->context->id === 'about',
				'url' => ['/about/about-us'],
				'options'=>['class'=>'main-menu__item main-menu__item_submenu'],
				'submenuTemplate' => "<ul class='main-menu__submenu'>{items}</ul>",
				'items' => [
					['label' => 'Отзывы', 'url' => ['/about/reviews']],
					['label' => 'О нас', 'url' => ['/about/about-us']],
					['label' => 'Наша команда', 'url' => ['/about/team']],
					['label' => 'Карьера', 'url' => ['/about/career']],
					['label' => 'Сертификаты', 'url' => ['/about/certificate']],
					['label' => 'Новости', 'url' => ['/news']],
					['label' => 'Статьи', 'url' => ['/articles']],
				],
			],
			[
				'label' => 'Услуги',
				'url' => ['/services'],
				'active' => $this->context->id === 'services',
				'options'=>['class'=>'main-menu__item main-menu__item_submenu'],
				'submenuTemplate' => "<ul class='main-menu__submenu'>{items}</ul>",
				'items' => $servicesMenu['items']
			],
			[
				'label' => 'Продукция',
				'url' => ['/production/index'],
				'active' => $this->context->id === 'production',
				'options'=>['class'=>'main-menu__item main-menu__item_submenu'],
				'submenuTemplate' => "<ul class='main-menu__submenu'>{items}</ul>",
				'items' => $categoryMenu['items']
			],
			[
				'label' => 'Акции',
				'url' => ['promotion/current'],
				'active' => $this->context->id === 'promotion',
				'options' => [
					'class' => 'main-menu__item main-menu__item_shares'
				]
			],
			[
				'label' => 'Сотрудничество',
				'active' => $this->context->id === 'cooperation',
				'options'=>['class'=>'main-menu__item main-menu__item_submenu'],
				'submenuTemplate' => "<ul class='main-menu__submenu'>{items}</ul>",
				'items' => [
					['label' => 'Дилерство', 'url' => ['/cooperation/dealership']],
					['label' => 'Корпоративным клиентам', 'url' => ['/cooperation/corporate-client']],
				],
			],
			[
				'label' => 'Наши работы',
				'url' => ['/our-works/'],
				'active' => $this->context->id === 'our-works',
			],
			['label' => 'Служба заботы о клиентах', 'url' => ['/customer-care-service/index']],
			['label' => 'Где купить', 'url' => ['/where-to-buy/index']],
		],
		'itemOptions'=>['class'=>'main-menu__item'],
		'activeCssClass'=>'is-active',
	]); ?>
</nav>