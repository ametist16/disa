<?php
namespace app\modules\productions\api;

use Yii;
use yii\easyii\components\API;
use yii\easyii\models\Photo;
use app\modules\productions\models\Item;
use yii\helpers\Url;

class ItemObject extends \yii\easyii\components\ApiObject
{
    public $slug;
    public $image;
    public $data;
    public $category_id;
    public $available;
    public $discount;
    public $time;

    private $_photos;

    public function getTitle(){
        return LIVE_EDIT ? API::liveEdit($this->model->title, $this->editLink) : $this->model->title;
    }
    public function getSubtitle(){
        return LIVE_EDIT ? API::liveEdit($this->model->subtitle, $this->editLink) : $this->model->subtitle;
    }

    public function getDescription(){
        return LIVE_EDIT ? API::liveEdit($this->model->description, $this->editLink, 'div') : $this->model->description;
    }

    public function getCat(){
        return Catalog::cats()[$this->category_id];
    }

    public function getPrice(){
        return $this->discount ? round($this->model->price * (1 - $this->discount / 100) ) : $this->model->price;
    }

    public function getPrice_str(){
        return LIVE_EDIT ? API::liveEdit($this->model->price_str, $this->editLink) : $this->model->price_str;
    }

    public function getAdvantages(){
        $advantages = json_decode($this->model->advantages);
        $advantages = $advantages ?: [];

        return array_filter($advantages, function ($item) {
            return $item;
        });
    }
    public function getProps(){
        $props = json_decode($this->model->props);
        $props = $props ?: [];

        return array_filter($props, function ($item) {
            return $item;
        });
    }

    public function getOldPrice(){
        return $this->model->price;
    }

    public function getDate(){
        return Yii::$app->formatter->asDate($this->time);
    }

    public function getPhotos()
    {
        if(!$this->_photos){
            $this->_photos = [];

            foreach(Photo::find()->where(['class' => Item::className(), 'item_id' => $this->id])->sort()->all() as $model){
                $this->_photos[] = new PhotoObject($model);
            }
        }
        return $this->_photos;
    }

    public function getEditLink(){
        return Url::to(['/admin/catalog/items/edit/', 'id' => $this->id]);
    }
}