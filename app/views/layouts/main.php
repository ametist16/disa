<?php
use yii\helpers\Html;

$asset = \app\assets\AppAsset::register($this);
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html lang="<?= Yii::$app->language ?>">
	<head>
		<meta charset="<?= Yii::$app->charset ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<?= Html::csrfMetaTags() ?>
		<title><?= Html::encode($this->title) ?></title>
		<link rel="shortcut icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
		<link rel="icon" href="<?= $asset->baseUrl ?>/favicon.ico" type="image/x-icon">
		<?php $this->head() ?>

		<?/*
		<link rel="stylesheet" href="/f/css/slick.css">
		<link rel="stylesheet" href="/f/css/slick-theme.css">
		<link rel="stylesheet" href="/f/css/styles.css">
		*/?>
		
<!-- Marquiz script start -->
<script src="//script.marquiz.ru/v1.js" type="application/javascript"></script>
<script>
document.addEventListener("DOMContentLoaded", function() {
  Marquiz.init({ 
    host: '//quiz.marquiz.ru', 
    id: '5fc6239e710f3a0044ded0ed', 
    autoOpen: 20, 
    autoOpenFreq: 'once', 
    openOnExit: true 
  });
});
</script>
<!-- Marquiz script end -->



		
	</head>
	<body>
	<?php $this->beginBody() ?>

	<?= $this->render('_header', []);?>

	<?= $content ?>

	<?= $this->render('_footer', []);?>
	<?= $this->render('_popups', []);?>

	<?php $this->endBody() ?>
	<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
	<?/*
		<script src="/app/media/js/jquery.modulecreator.min.js"></script>
		<script src="/app/media/js/slick.min.js"></script>
		<script src="/app/media/js/app.js"></script>
	*/?>
	<script>
        (function(w,d,u){
                var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/60000|0);
                var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h);
        })(window,document,'https://cdn.bitrix24.ru/b9517263/crm/site_button/loader_1_vgtur0.js');
</script>


<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

   ym(32137425, "init", {
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/32137425" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->






	</body>
	</html>
<?php $this->endPage() ?>