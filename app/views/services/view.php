<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Breadcrumbs;
	$this->title = $item->seo('title', $item->model->title);
	$this->registerMetaTag(
		['name' => 'description',
		'content' => $item->seo('description', $item->model->title)
	]);

	$curUrl = $this->context->actionParams['slug'];

	$this->params['breadcrumbs'][] = ['label' => 'Услуги', 'url' => ['/services']];
	$this->params['breadcrumbs'][] = ['label' => $item->title];
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="container">
		<h1 class="section__title"><?= Page::title($item->seo('h1', $item->title)); ?></h1>

		<div class="stock-item__img-wrap">
			<img src="<?=$item->image;?>" alt="">
		</div>

		<div class="page__text">
			<?= $item->text; ?>
		</div>

	</div>
</section>

<? // Разный контент форм для разных страниц услуг с привязкой к id ?>
<? if($curUrl === 'professional-consultation'): ?>
	<?= $this->render('_form', [
		'title' => 'хотите получить <span class="section__title_blue">консультацию</span>',
		'text' => 'Если Вы хотите получить консультацию, то свяжитесь с нами по телефону или заполните форму и напишите свой вопрос ниже.'
	]);?>
<? elseif($curUrl === 'warranty'): ?>
	<?= $this->render('_form-warranty', [
		'title' => 'Записаться на <span class="section__title_blue">гарантийное обслуживание</span>',
		'text' => 'Если Вы хотите бесплатный замер, то свяжитесь с нами по телефону или заполните форму и напишите свой вопрос ниже.'
	]);?>
<? elseif($curUrl === 'free-froze'): ?>
	<?= $this->render('_form', [
		'title' => 'хотите получить <span class="section__title_blue">бесплатный замер</span>',
		'text' => 'Если Вы хотите бесплатный замер, то свяжитесь с нами по телефону или заполните форму и напишите свой вопрос ниже.'
	]);?>
<? else: ?>
	<?= $this->render('_form', [
		'title' => 'хотите получить <span class="section__title_blue">консультацию</span>',
		'text' => 'Если Вы хотите получить консультацию, то свяжитесь с нами по телефону или заполните форму и напишите свой вопрос ниже.'
	]);?>
<? endif; ?>
