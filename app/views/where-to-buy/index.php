<?php
use app\modules\pages\api\Page;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = $page->seo('title', $page->model->title);
$this->registerMetaTag(
	['name' => 'description',
	'content' => $page->seo('description', $page->model->title)
]);

$this->params['breadcrumbs'][] = ['label' => 'Где купить'];

?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="container">
		<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>

		<div class="contacts">
			<div class="contacts__header">Контактные данные</div>
			<div class="contacts__mail">E-mail: <a href="mailto:disa.okna@gmail.com">disa.okna@gmail.com</a></div>
			<div class="contacts__list">
				<? foreach ($shops as $key => $shop): ?>
					<div class="contacts__item">
						<div class="contacts__data">
							<div class="contacts__row">
								<div class="contacts__icon"><img src="/f/i/icon-contact-map.png" alt=""></div>
								<div class="contacts__content">
									<div class="contacts__name">Адрес:</div>
									<div class="contacts__value">
										<?=$shop['city'];?>, <?=$shop['district'];?>
										<?=$shop['address'];?>
									</div>
								</div>
							</div>
							<div class="contacts__row">
								<div class="contacts__icon"><img src="/f/i/icon-contact-phone.png" alt=""></div>
								<div class="contacts__content">
									<div class="contacts__name">Телефон:</div>
									<div class="contacts__value">
										<?=$shop['phone'];?>
									</div>
								</div>
							</div>
							<div class="contacts__row">
								<div class="contacts__icon"><img src="/f/i/icon-contact-grafic.png" alt=""></div>
								<div class="contacts__content">
									<div class="contacts__name">График работы:</div>
									<div class="contacts__value">
										<pre><?=$shop['worktime'];?></pre>
									</div>
								</div>
							</div>
							<div class="contacts__row">
								<div class="contacts__icon"><img src="/f/i/icon-contact-manager.png" alt=""></div>
								<div class="contacts__content">
									<div class="contacts__name">Менеджер:</div>
									<div class="contacts__value">
										<?=$shop['manager'];?>
									</div>
								</div>
							</div>
						</div>
						<div class="contacts__map-wrapper">
							<div class="js__map contacts__map" data-map-center=<?=$shop['centermap'];?> data-map-marker=<?=$shop['coordinates'];?>>
							</div>
						</div>
					</div>
				<? endforeach; ?>
			</div>
			
		</div>
	</div>
</section>
