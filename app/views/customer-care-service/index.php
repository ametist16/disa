<?php
	use yii\widgets\Breadcrumbs;
	use app\modules\pages\api\Page;
	$this->title = $page->seo('title', $page->model->title);
	$this->registerMetaTag(
		['name' => 'description',
		'content' => $page->seo('description', $page->model->title)
	]);

	$this->params['breadcrumbs'][] = ['label' => 'Служба заботы о клиентах'];
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="section__inner">
		<div class="container">
			<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>

			<div class="section__text section__text_left">
				<?=$page->text;?>
			</div>

		</div>
	</div>
</section>

<section class="section section_gray section_get-stock">
	<div class="section__inner">
		<div class="container">
			<div class="customer-tabs js__tab">
				<div class="customer-tabs__tab-head">
					<div class="customer-tabs__tab-head-item js__tab-btn is-active">Задать вопрос</div>
					<div class="customer-tabs__tab-head-item js__tab-btn">Пожаловаться</div>
					<div class="customer-tabs__tab-head-item js__tab-btn">Обратиться к директору</div>
				</div>
				<div class="customer-tabs__tab-list">
					<div class="customer-tabs__tab is-active js__tab-item">
						<form action="" class="get-stock__form js__form">
							<div class="get-stock__row">
								<div class="get-stock__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
										</div>
									</div>
								</div>
								<div class="get-stock__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
										</div>
									</div>
								</div>
							</div>
							<div class="get-stock__row">
								<div class="get-stock__textarea">
									<div class="textarea">
										<div class="textarea__wrapper">
											<textarea name="message" id="" placeholder="Сообщение" class="textarea__textarea" data-type="requared"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="get-stock__row">
								<div class="get-stock__checkbox">
									<label class="checkbox">
										<input type="checkbox" class="checkbox__input" data-type="checkbox">
										<div class="checkbox__icon"></div>
										Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
									</label>
								</div>
								<div class="get-stock__btn">
									<button class="btn">Отправить</button>
								</div>
							</div>
							<div class="response"></div>
						</form>
					</div>
					<div class="customer-tabs__tab js__tab-item">
						<form action="" class="get-stock__form js__form">
							<div class="get-stock__row">
								<div class="get-stock__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
										</div>
									</div>
								</div>
								<div class="get-stock__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
										</div>
									</div>
								</div>
							</div>
							<div class="get-stock__row">
								<div class="get-stock__textarea">
									<div class="textarea">
										<div class="textarea__wrapper">
											<textarea name="message" id="" placeholder="Сообщение" class="textarea__textarea" data-type="requared"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="get-stock__row">
								<div class="get-stock__checkbox">
									<label class="checkbox">
										<input type="checkbox" class="checkbox__input" data-type="checkbox">
										<div class="checkbox__icon"></div>
										Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
									</label>
								</div>
								<div class="get-stock__btn">
									<button class="btn">Отправить</button>
								</div>
							</div>
							<div class="response"></div>
						</form>
					</div>
					<div class="customer-tabs__tab js__tab-item">
						<form action="" class="get-stock__form js__form">
							<div class="get-stock__row">
								<div class="get-stock__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
										</div>
									</div>
								</div>
								<div class="get-stock__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
										</div>
									</div>
								</div>
							</div>
							<div class="get-stock__row">
								<div class="get-stock__textarea">
									<div class="textarea">
										<div class="textarea__wrapper">
											<textarea name="message" id="" placeholder="Сообщение" class="textarea__textarea" data-type="requared"></textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="get-stock__row">
								<div class="get-stock__checkbox">
									<label class="checkbox">
										<input type="checkbox" class="checkbox__input" data-type="checkbox">
										<div class="checkbox__icon"></div>
										Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
									</label>
								</div>
								<div class="get-stock__btn">
									<button class="btn">Отправить</button>
								</div>
							</div>
							<div class="response"></div>
						</form>
					</div>
				</div>
			</div>

			
		</div>
	</div>
</section>