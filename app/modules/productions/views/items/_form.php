<?php
use yii\easyii\helpers\Image;
use yii\easyii\widgets\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;

$settings = $this->context->module->settings;
$module = $this->context->module->id;
$advantages = $model->advantages ? json_decode($model->advantages) : [];
$props = $model->props ? json_decode($model->props) : [];

?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>

<div class="row">
    <div class="col-sm-6">
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'subtitle') ?>
        <?= $form->field($model, 'price_str') ?>
    </div>
    <div class="col-sm-6">
        <?php if($settings['itemThumb']) : ?>
            <?php if($model->image) : ?>
                <img src="<?= Image::thumb($model->image, 240) ?>">
                <a href="<?= Url::to(['/admin/'.$module.'/items/clear-image', 'id' => $model->primaryKey]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
            <?php endif; ?>
            <?= $form->field($model, 'image')->fileInput() ?>
        <?php endif; ?>
    </div>
</div>

<hr>
<label class="control-label">
    Список преимуществ:
</label>
<br>
<div class="js__advantages">
    <? if(count($advantages) > 0): ?>
        <? foreach ($advantages as $key => $advantage): ?>
            <div class="row">
                <div class="col-sm-11">
                    <div class="form-group">
                        <input class="form-control" name="advantages[<?= $key; ?>][name]" type="text" value="<?= $advantage->name; ?>">
                    </div>
                </div>
                <div class="col-sm-1">
                    <a class="btn btn-default js__remove-element" title="Удалить запись">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
    <? endif; ?>
</div>

<div class="btn btn-primary js__add-advantage">Добавить преимущество</div>

<hr>

<label class="control-label">
    Список свойств:
</label>
<br>
<div class="js__props">
    <? if(count($props) > 0): ?>
        <? foreach ($props as $key => $advantage): ?>
            <div class="row">
                <div class="col-sm-5">
                    <div class="form-group">
                        <input class="form-control" name="props[<?= $key; ?>][name]" type="text" value="<?= $advantage->name; ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <input class="form-control" name="props[<?= $key; ?>][value]" type="text" value="<?= $advantage->value; ?>">
                    </div>
                </div>
                <div class="col-sm-1">
                    <a class="btn btn-default js__remove-element" title="Удалить запись">
                        <span class="glyphicon glyphicon-remove"></span>
                    </a>
                </div>
            </div>
        <? endforeach; ?>
    <? endif; ?>
</div>

<div class="btn btn-primary js__add-prop">Добавить свойство</div>

<hr>
<?//= $form->field($model, 'available') ?>
<?//= $form->field($model, 'discount') ?>

<?= $form->field($model, 'warranty') ?>

<?= $form->field($model, 'installment') ?>

<?= $dataForm ?>

<?php if($settings['itemDescription']) : ?>
    <?= $form->field($model, 'description')->widget(Redactor::className(),[
        'options' => [
            'minHeight' => 200,
            'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'productions'], true),
            'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'productions'], true),
            'plugins' => ['fullscreen']
        ]
    ]) ?>
<?php endif; ?>


<?//= $form->field($model, 'time')->widget(DateTimePicker::className()); ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>

<script>
    $(function () {
        var Advantages = {
            init: function () {
                $(document).on('click', '.js__add-advantage', this.addElement)
            },
            lastCount: <?= count($advantages); ?>,
            addElement: function () {
                var element = Advantages.createElement();

                element.appendTo('.js__advantages')
            },
            createElement: function () {
                var tpl = $(`<div class="row">
                    <div class="col-sm-11">
                        <div class="form-group">
                            <input class="form-control" name="advantages[${Advantages.lastCount}][name]" type="text">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <a class="btn btn-default js__remove-element" title="Удалить запись">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </div>`);

                Advantages.lastCount++;

                return tpl;
            }
        };

        Advantages.init();

        var Props = {
            init: function () {
                $(document).on('click', '.js__add-prop', this.addElement)
                $(document).on('click', '.js__remove-element', this.removeElement)
            },
            lastCount: <?= count($props); ?>,
            addElement: function () {
                var element = Props.createElement();

                element.appendTo('.js__props')
            },
            createElement: function () {
                var tpl = $(`<div class="row">
                    <div class="col-sm-5">
                        <div class="form-group">
                            <input class="form-control" name="props[${Props.lastCount}][name]" type="text">
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <input class="form-control" name="props[${Props.lastCount}][value]" type="text">
                        </div>
                    </div>
                    <div class="col-sm-1">
                        <a class="btn btn-default js__remove-element" title="Удалить запись">
                            <span class="glyphicon glyphicon-remove"></span>
                        </a>
                    </div>
                </div>`);

                Props.lastCount++;

                return tpl;
            },
            removeElement (event) {
                var element = $(event.currentTarget).closest('.row')

                element.remove();
            }
        };

        Props.init();
    });
</script>