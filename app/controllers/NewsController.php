<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\pages\api\Page;
use app\modules\companynews\api\News;

class NewsController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $page = Page::get('page-news');
        $news = News::items(['pagination' => ['pageSize' => 10]]);

        return $this->render('index', [
            'page' => $page,
            'news' => $news
        ]);
    }

    public function actionView($slug)
    {
        $item = News::get($slug);

        News::plugin();

        return $this->render('view', [
            'item' => $item
        ]);
    }
}