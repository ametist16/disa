<div class="feedback-slider">
	<div class="feedback-slider__inner">
		<div class="feedback-slider__list">

			<? foreach ($reviews as $key => $review): ?>
				<div class="reviews__item">
					<div class="reviews__name"><?=$review->title;?></div>
					<div class="reviews__date"><?=$review->date;?></div>

						<div class="reviews__text">
							<?=$review->text;?>
						</div>

						<? if($review->image): ?>
							<div class="reviews__origin">
								<div class="reviews__origin-icon"><img src="/f/i/icon-review-origin.png" alt=""></div>
								<a href="<?=$review->image;?>" class="easyii-box">Посмотреть оригинал отзыва</a>
							</div>
						<? endif; ?>

				</div>
			<? endforeach; ?>

		</div>
		<div class="feedback-slider__btn">
			<a href="/about/reviews" class="btn">посмотреть все отзывы</a>
		</div>
	</div>
</div>