<?
	use yii\easyii\helpers\Image;

	$works = [
		'/f/i/works/information_items_258.png',
		'/f/i/works/information_items_288.jpg',
		'/f/i/works/information_items_430.jpg',
		'/f/i/works/information_items_497.jpg',
		'/f/i/works/information_items_528.png',
		'/f/i/works/information_items_539.jpg',
	];
?>

<div class="works-slider">
	<div class="works-slider__inner">
		<div class="works-slider__list">
			<?/* foreach ($works as $key => $work): ?>
			<div class="works-slider__item">
				<img src="<?= $work->image; ?>" alt="" class="works-slider__img">
			</div>
			<? endforeach; */?>

			<? foreach ($works as $key => $img): ?>
			<div class="works-slider__item">
				<img src="<?= Image::thumb($img, 880, 480, true); ?>" alt="" class="works-slider__img">
			</div>
			<? endforeach; ?>

		</div>
		<div class="works-slider__btn">
			<a href="/our-works/okna-pvh" class="btn">посмотреть все фотографии</a>
		</div>
	</div>
</div>