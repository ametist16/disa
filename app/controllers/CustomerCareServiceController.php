<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\pages\api\Page;

class CustomerCareServiceController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $page = Page::get('page-customer-care-service');

        return $this->render('index', [
            'page' => $page
        ]);
    }
}