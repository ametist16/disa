<?php
$this->title = Yii::t('easyii/services', 'Create news');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>