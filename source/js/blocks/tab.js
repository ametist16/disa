var Tab = {
	init: function () {
		$(document).on('click', Tab.options.tabBtn, this.onChangeTab);
	},
	options: {
		tab: '.js__tab',
		tabItem: '.js__tab-item',
		tabBtn: '.js__tab-btn',
		classActive: 'is-active',
	},
	onChangeTab (event) {
		var btn = $(event.currentTarget);
		var index = btn.index();
		var tabs = btn.closest(Tab.options.tab);

		Tab.setTab(tabs, index);
		$(document).trigger('changetab');
	},
	setTab (tabs, index) {
		var allTabs = tabs.find(Tab.options.tabItem);
		var newTab = allTabs.eq(index);
		var allBtn = tabs.find(Tab.options.tabBtn);
		var newBtn = allBtn.eq(index);

		allBtn.removeClass(Tab.options.classActive);
		newBtn.addClass(Tab.options.classActive);

		allTabs.removeClass(Tab.options.classActive).hide();
		newTab.addClass(Tab.options.classActive).show();
	}
};

$(function () {
	Tab.init();
});