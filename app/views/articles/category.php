<?php
	use yii\widgets\Breadcrumbs;
	use yii\widgets\Menu;
	use yii\easyii\helpers\Image;
	use app\modules\articles\api\Article as Articles;
?>
<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="section__inner">
		
		<div class="container">
			<h1 class="section__title">Статьи</h1>

			<!-- Меню подкатегорий -->
			<? if ($subMenu): ?>
				<nav>
					<?= Menu::widget($subMenu); ?>
				</nav>
			<? endif; ?>

			<!-- Элементы категории -->
			<? if ($items): ?>
				<div class="stock-list">
					<div class="stock-list__wrapper">
						<div class="stock-list__list">

							<? foreach($items as $item): ?>

								<? $img = $item->model->image_preview ? $item->model->image_preview : $item->image; ?>
								<a href="/articles/<?= $category->slug; ?>/<?= $item->slug; ?>" class="stock-list__item">
									<div class="stock-list__img">
										<img src="<?= Image::thumb($img, 533, 284, true); ?>" alt="">
									</div>
									<div class="stock-list__title"><?= $item->title; ?></div>
									<div class="stock-list__text"><?= $item->short; ?></div>
								</a>

							<? endforeach; ?>

						</div>
					</div>
				</div>
			<? endif; ?>

			<!-- Пагинация -->
			<?= Articles::pages() ?>
		</div>
	</div>
</section>
