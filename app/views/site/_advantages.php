<div class="advantages">
	<div class="advantages__list">
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/proizvodstvo.png" alt=""></div>
				<div class="advantages__title">Свой производственный <br>комплекс с системой ОТК</div>
			</div>
			<div class="advantages__body">
				<div class="advantages__text">Производим изделия из пвх и алюминия любой сложности, том числе нестандартных конфигураций. Каждый этап производства проходит под контролем собственной службы качества. Сотрудничаем только с поставщиками, проверенными временем.</div>
			</div>
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/garantiya.png" alt=""></div>
				<div class="advantages__title">Письменная гарантия <br>качества 10 лет</div>
			</div>
			<div class="advantages__body">
				<div class="advantages__text">Мы несем ответственность за качество продукции (гарантия предоставляется на изделия из ПВХ с монтажом)</div>
			</div>
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/innovacii.png" alt=""></div>
				<div class="advantages__title">Инновации для Вашего <br>удобства</div>
			</div>
			<div class="advantages__body">
				<div class="advantages__text">Использование технологии  теплого края для производства стеклопакетов. Вся продукция, выпускаемая на нашем предприятии, соответствует нормам ГОСТа.</div>
			</div>
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/skidki.png" alt=""></div>
				<div class="advantages__title">Гибкая система <br>скидок</div>
			</div>
			<div class="advantages__body">
				<div class="advantages__text">Мы постоянно проводим акции и спецпредложения. Вы можете купить ПВХ окна недорого от производителя в Брянске и Брянской области и дополнительно получить приятный бонус.</div>
			</div>
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/montag.png" alt=""></div>
				<div class="advantages__title">Профессиональный монтаж <br>в любое время года</div>
			</div>
			<div class="advantages__body">
				<div class="advantages__text">Демонтаж и монтаж осуществят сертифицированные, штатные сотрудники, с солидным опытом работы.</div>
			</div>
		</div>
		<div class="advantages__item">
			<div class="advantages__head">
				<div class="advantages__img"><img src="/f/i/advantages/compleks.png" alt=""></div>
				<div class="advantages__title">Комплексный подход <br>к каждому клиенту</div>
			</div>
			<div class="advantages__body">
				<div class="advantages__text">Квалифицированный подход к решению задач любой сложности для оптимального выбора и быстрого решения любого вопроса.</div>
			</div>
		</div>
	</div>
</div>