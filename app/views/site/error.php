<?php
use yii\helpers\Html;
$this->title = $name;
?>

<div class="section">
	<div class="container">
		<div class="section__title"><?= Html::encode($this->title) ?></div>

		<div class="alert alert-danger">
			<?= nl2br(Html::encode($message)) ?>
		</div>

		<p>
			The above error occurred while the Web server was processing your request.
		</p>
		<p>
			Please contact us if you think this is a server error. Thank you.
		</p>

	</div>
</div>
