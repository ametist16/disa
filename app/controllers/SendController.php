<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\easyii\models\Setting;

class SendController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $data = Yii::$app->request->get();

        $text = '';
        foreach ($data as $key => $value) {
            $text .= "$key : $value \n";
        }

        $to = Setting::get('admin_email');
        $subject = "От поситителя сайта disa-okna.ru";

        $header = "MIME-Version: 1.0\r\n";
        $header .= "Content-type: text/html; charset=utf-8\r\n";

        $sending = mail($to, $subject, $this->getTpl($data), $header);

        if ($sending) {
            echo 'Ваше сообщение успешно отправлено!';
        } else {
            echo 'Сообщение отправить не удалось. Свяжитесь с нами по телефону.';
        }
    }

    private function getTpl ($data)
    {
        $content = '<table width="100%" style="background: #eee; border-collapse: collapse;">
            <tbody>
                <tr>
                    <td align="center" style="padding: 40px 20px;">

                        <table width="100%" style="border-collapse: collapse; background: #fff; max-width: 600px;">
                            <tbody>
                                <tr>
                                    <td style="padding: 10px 20px;" align="center">
                                        <!-- <img width="320px" src="http://disa-okna.ru/f/i/top-logo.png" alt=""> -->
                                        <a href="http://disa-okna.ru">
                                            <img width="320px" src="http://mailrazd.bget.ru/f/i/top-logo.png" alt="">
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="100%" style="border-collapse: collapse; max-width: 600px;">
                            <tbody>
                                <tr>
                                    <td style="padding: 20px; background: #fff;"></td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="100%" style="background: #fff; border-collapse: collapse; max-width: 600px;">
                            <tbody>';

        foreach ($data as $key => $value) {
            $content = $content . $this->getRow($key, $value);
        }

        $content = $content.'</tbody>
                        </table>

                        <table width="100%" style="border-collapse: collapse; max-width: 600px;">
                            <tbody>
                                <tr>
                                    <td style="padding: 20px; background: #fff;"></td>
                                </tr>
                            </tbody>
                        </table>

                        <table width="100%" style="border-collapse: collapse; max-width: 600px; background: #f76228;">
                            <tbody>
                                <tr>
                                    <td style="padding: 20px 20px;" align="center">
                                        <a href="http://disa-okna.ru" style="color: #fff; text-decoration: none;">disa-okna.ru</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </td>
                </tr>
            </tbody>
        </table>';

        return $content;
    }

    private function getRow ($label, $value)
    {
        return ('<tr>
            <td style="padding: 10px 20px; border: 1px solid #ccc;">'.$label.'</td>
            <td style="padding: 10px 20px; border: 1px solid #ccc;">'.$value.'</td>
        </tr>');
    }
}
