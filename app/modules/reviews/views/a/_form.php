<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;

$module = $this->context->module->id;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>


<div class="row">
    <div class="col-sm-5">
        <?= $form->field($model, 'title') ?>
        <?= $form->field($model, 'time')->widget(DateTimePicker::className(), [
            'options' => [
                'format' => 'D.MM.YYYY',
            ]
        ]); ?>
        <?= $form->field($model, 'video')->textarea(['rows' => 4,])->hint('Если запонлнить это поле, то текст и загруженное изображение не будет отображаться на сайте.') ?>
    </div>
    <div class="col-sm-7">
        <?php if($this->context->module->settings['enableThumb']) : ?>
            <?php if($model->image) : ?>
                <img src="<?= Image::thumb($model->image, 240) ?>">
                <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->news_id]) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
            <?php endif; ?>
            <?= $form->field($model, 'image')->fileInput() ?>
        <?php endif; ?>

        <?php if($model->video) : ?>
            <iframe width="500" height="280" src="<?= $model->video; ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
        <?php endif; ?>
    </div>
</div>


<?= $form->field($model, 'text')->widget(Redactor::className(),[
    'options' => [
        'minHeight' => 300,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'reviews']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'reviews']),
        'plugins' => ['fullscreen']
    ]
]) ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
