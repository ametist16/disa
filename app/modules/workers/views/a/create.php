<?php
$this->title = Yii::t('easyii/workers', 'Create news');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>