var MobileMenu = {
	init: function () {
		$(document).on('click', MobileMenu.options.menuBtn, this.onClickMenu);

		if ($(window).width() < 960) {
			$(document).on('click', '.main-menu__item_submenu', this.toogleSubmenu);
		}
	},
	options: {
		menuBtn: '.header-top__menu-mob',
		menu: '.main-menu',
		classActive: 'is-active',
	},
	onClickMenu (event) {
		var btn = $(event.currentTarget);
		
		MobileMenu.toggleMenu(btn);
	},
	toggleMenu (btn) {

		btn.toggleClass(MobileMenu.options.classActive);
		$(MobileMenu.options.menu).toggleClass(MobileMenu.options.classActive);
	},
	toogleSubmenu(event) {
		var item = $(event.currentTarget),
			submenu = item.find('.main-menu__submenu');

		if (item.hasClass('is-opened')) {
			item.removeClass('is-opened');
			submenu.stop().slideUp();
		} else {
			item.addClass('is-opened');
			submenu.stop().slideDown();
		}

	}
};

$(function () {
	MobileMenu.init();
});