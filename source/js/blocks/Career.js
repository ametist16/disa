var Career = {
	init: function () {
		$(document).on('click', Career.options.careerBtn, this.onClickBtn);
	},
	options: {
		careerItem: '.js__career',
		careerBtn: '.js__career-btn',
		careerBody: '.js__career-body',
		classActive: 'is-opened',
	},
	onClickBtn (event) {
		var btn = $(event.currentTarget);
		
		Career.toggleBody(btn);
	},
	toggleBody (btn) {

		var item = btn.closest(Career.options.careerItem);
		var body = item.find(Career.options.careerBody);

		btn.toggleClass(Career.options.classActive);

		if (btn.hasClass(Career.options.classActive)) {
			body.slideDown(300);
		} else {
			body.slideUp(300);
		}
	}
};

$(function () {
	Career.init();
});