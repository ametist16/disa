<section class="section section_gray section_corporation-form">
	<div class="section__inner">
		<div class="container">
			<div class="section__title">Надеемся в Вашем лице приобрести <span class="section__title_blue">надежного партнера.</span><br>Будем рады <span class="section__title_blue">сотрудничеству</span> с вами!</div>
			<form action="" class="get-stock__form js__form">
				<div class="get-stock__row">
					<div class="get-stock__input">
						<div class="input">
							<div class="input__wrapper">
								<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
							</div>
						</div>
					</div>
					<div class="get-stock__input">
						<div class="input">
							<div class="input__wrapper">
								<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
							</div>
						</div>
					</div>
				</div>
				<div class="get-stock__row">
					<div class="get-stock__textarea">
						<div class="textarea">
							<div class="textarea__wrapper">
								<textarea name="message" id="" placeholder="Сообщение" class="textarea__textarea" data-type="requared"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="get-stock__row">
					<div class="get-stock__checkbox">
						<label class="checkbox">
							<input type="checkbox" class="checkbox__input" data-type="checkbox">
							<div class="checkbox__icon"></div>
							Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
						</label>
					</div>
					<div class="get-stock__btn">
						<button class="btn">Отправить</button>
					</div>
				</div>
				<div class="response"></div>
			</form>
		</div>
	</div>
</section>