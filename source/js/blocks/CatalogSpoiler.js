var CatalogSpoiler = {
	init: function () {
		$(document).on('click', CatalogSpoiler.options.catalogBtn, this.onChangeCatalog);

		
		
		if ($(CatalogSpoiler.options.catalogBtn).length) {
			$(CatalogSpoiler.options.catalogBtn).each(function (index, item) {
				setTimeout(function() { 

					$(item).trigger('click');
				}, 2000);
			});
		}
		
	},
	options: {
		catalog: '.js__catalog-spoiler',
		catalogItem: '.js__catalog-spoiler-item',
		catalogBtn: '.js__catalog-spoiler-btn',
		classActive: 'is-hidden',
	},
	onChangeCatalog (event) {
		event.preventDefault();
		var btn = $(event.currentTarget);
		
		var catalog = btn.closest(CatalogSpoiler.options.catalog);

		CatalogSpoiler.toogleItems(catalog, btn);
	},
	toogleItems (catalog, btn) {

		var items = catalog.find(CatalogSpoiler.options.catalogItem);

		if (btn.hasClass(CatalogSpoiler.options.classActive)) {
			items.each(function (index, item) {
				var item = $(item);
				if (index > 5) {
					item.slideDown();
				}
			});

			btn.removeClass(CatalogSpoiler.options.classActive);
			btn.text(btn.data('open-text'));
		} else {
			items.each(function (index, item) {
				var item = $(item);
				if (index > 5) {
					item.slideUp();
				}
			});
			btn.addClass(CatalogSpoiler.options.classActive);
			btn.text(btn.data('hide-text'));
		}
	}
};

$(function () {
	CatalogSpoiler.init();
});