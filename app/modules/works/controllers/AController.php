<?php
namespace app\modules\works\controllers;

use yii\easyii\components\CategoryController;
use app\modules\works\models\Category;

class AController extends CategoryController
{
    public $categoryClass = 'app\modules\works\models\Category';
    public $moduleName = 'works';
    public $viewRoute = '/a/photos';

    public function actionPhotos($id)
    {
        if(!($model = Category::findOne($id))){
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }
}