
<div class="aside-block">
	<div class="aside-block__inner">
		<div class="aside-block__title">Отзывы наших клиентов</div>
		<div class="aside-block__content">
			
			<div class="reviews reviews_catalog">
				<div class="reviews__list">
					<? foreach ($reviews as $key => $review): ?>
						<div class="reviews__item reviews__item_catalog">
							<div class="reviews__name"><?=$review->title;?></div>
							<div class="reviews__date"><?=$review->date;?></div>

							<div class="reviews__text">
								<?=$review->text;?>
							</div>

							<? if($review->image): ?>
								<div class="reviews__origin">
									<div class="reviews__origin-icon"><img src="/f/i/icon-review-origin.png" alt=""></div>
									<a href="<?=$review->image;?>" class="easyii-box">Посмотреть оригинал отзыва</a>
								</div>
							<? endif; ?>
						</div>
					<? endforeach; ?>
				</div>
				<a href="/about/reviews" class="reviews__all-link">Все отзывы</a>
			</div>

		</div>
	</div>
</div>
