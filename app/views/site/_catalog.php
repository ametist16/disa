<?php
	use yii\helpers\Url;
?>

<section class="section">
	<div class="section__inner">
		<div class="container">
			<div class="section__title">каталог <span class="section__title_blue">продукции</span></div>
			<div class="section__text">В нашей компании вы можете купить окна из  ПВХ и алюминия уже сегодня! Мы располагаем собственным производством и предоставляем клиентам огромный ассортимент светопрозрачных конструкций из алюминиевого и ПВХ профилей. Всегда предложим вам лучшее решение, идеально подобранное под ваше помещение: частный дом, квартиру, офис, магазин или салон красоты. Познакомиться с ценами на пластиковые окна в Брянске вы можете непосредственно на этом официальном сайте.</div>

			<div class="catalog js__catalog-spoiler">
				<div class="catalog__list">
					<? foreach ($categoryList as $key => $category): ?>
						<a href="<?= Url::toRoute(['production/category', 'cat' => $category->slug]); ?>" class="catalog__item js__catalog-spoiler-item" style="background-image: url('<?= $category->image ?>');">
							<div class="catalog__body">
								<div class="catalog__icon">
									<img src="<?= $category->icon ?>" alt="">
								</div>
								<div class="catalog__title"><?= $category->title ?></div>
							</div>
						</a>
					<? endforeach; ?>
				</div>

				<div class="catalog__btn">
					<a href="#" class="btn js__catalog-spoiler-btn" data-open-text="Скрыть" data-hide-text="Показать еще" >Показать еще</a>
				</div>
			</div>

		</div>
	</div>
</section>
