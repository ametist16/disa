<?php

$params = require(__DIR__ . '/params.php');

$basePath =  dirname(__DIR__);
$webroot = dirname($basePath);

$config = [
    'id' => 'app',
    'basePath' => $basePath,
    'bootstrap' => ['log'],
    'language' => 'ru-RU',
    'runtimePath' => $webroot . '/runtime',
    'vendorPath' => $webroot . '/vendor',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Kvf:BliG"GP:Ijh[pIHG',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
        ],
        'urlManager' => [
            'rules' => [
                'our-works/<slug:[\w-]+>' => 'our-works/view',

                // Production
                'production' => 'production/index',
                'production/<cat:[\w-]+>' => 'production/category',
                'production/<cat:[\w-]+>/<sub:[\w-]+>' => 'production/category',

                // Promotion
                'promotion/<cat:[\w-]+>' => 'promotion/category',
                'promotion/<cat:[\w-]+>/<slug:[\w-]+>' => 'promotion/view',
                // Articles
                'articles' => 'articles/index',
                'articles/<cat:[\w-]+>' => 'articles/category',
                'articles/<cat:[\w-]+>/<slug:[\w-]+>' => 'articles/view',
                // News
                'news' => 'news/index',
                'news/<slug:[\w-]+>' => 'news/view',

                // Services
                'services' => 'services/index',
                'services/<slug:[\w-]+>' => 'services/view',

                '<controller(customer-care-service|where-to-buy)>' => '<controller>/index',

                '<controller:\w+>' => '<controller>/index',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/view/<slug:[\w-]+>' => '<controller>/view',
                // '<controller:\w+>/cat/<slug:[\w-]+>' => '<controller>/cat',
            ],
        ],
        'assetManager' => [
            // uncomment the following line if you want to auto update your assets (unix hosting only)
            //'linkAssets' => true,
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js' => [YII_DEBUG ? 'jquery.js' : 'jquery.min.js'],
                ]
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
    
    $config['components']['db']['enableSchemaCache'] = false;
}

return array_merge_recursive($config, require($webroot . '/vendor/noumo/easyii/config/easyii.php'));