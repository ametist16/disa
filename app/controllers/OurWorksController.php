<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\modules\works\api\Gallery;
use yii\web\NotFoundHttpException;

class OurWorksController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    private function getMenu($categories, $className)
    {
        $categoryItems = [];

        foreach ($categories as $key => $value) {
            $categoryItems[] = [
                'label' => $value->title,
                'url' => '/' . $this->id. '/' . $value->slug,
                'active' => ($value->slug === $this->actionParams["slug"])
            ];
        }

        return  [
            'options' => [
                'class' => $className . '-menu__list'
            ],
            'items' => $categoryItems,
            'itemOptions'=>['class' => $className . '-menu__item'],
            'activeCssClass'=>'is-active',
        ];
    }

    public function actionIndex()
    {
        $categories = Gallery::tree();
        return $this->redirect('/our-works/'.$categories[0]->slug);
    }

    public function actionView($slug)
    {
        $category = Gallery::cat($slug);
        $categories = Gallery::tree();
        $categoryMenu = $this->getMenu($categories, 'catalog');

        if ($category === null) {
            throw new NotFoundHttpException;
        }

        Gallery::plugin();

        return $this->render('view', [
            'categoryMenu' => $categoryMenu,
            'category' => $category,
            'items' => $category->photos(['pagination' => ['pageSize' => 9]])
        ]);
    }
}
