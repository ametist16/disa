
$(function(){
	var oPopup = {
		init: function()
		{
			$(document).on('click','[data-popup-close]', oPopup.closeSelector);
			$(document).on('click','[data-popup-open]', oPopup.openSelector);

			$(window).on('resize', oPopup.checkHeight);
		},

		openSelector: function(event)
		{
			const jThis = $(event.currentTarget);
			const id = jThis.attr('data-popup-open');

			event.preventDefault();
			console.log(id);
			oPopup.open(id);
		},
		open: function (id)
		{
			const jPopUp = $('[data-popup-id="' + id + '"]');

			jPopUp.stop().fadeIn(200).css('z-index', 550).addClass('is-visible');
			$('body').addClass('is-overflow');

			oPopup.checkHeight();

			return false;
		},




		closeSelector: function(event)
		{
			const closer = $(event.currentTarget);
			const id = closer.attr('data-popup-close');

			event.preventDefault();
			oPopup.close(id);
		},
		close: function (id)
		{
			const jPopUp = $('[data-popup-id="' + id + '"]');

			jPopUp.stop().fadeOut(200).removeClass('is-visible');
			$('body').removeClass('is-overflow')

			return false;
		},




		checkHeight: function(){
			$('.popup:visible').each(function(){
				const jThis = $(this);

				if(jThis.outerHeight() >= $(window).height()){
					jThis.addClass('is-large');
				} else{
					jThis.removeClass('is-large');
				}
			});
		}
	}

	window.oPopup = oPopup;

	oPopup.init();
});