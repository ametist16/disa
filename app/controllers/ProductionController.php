<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\productions\api\Catalog;
use app\modules\pages\api\Page;
use app\modules\works\api\Gallery;
use app\modules\certificate\api\Certificate;
use app\modules\reviews\api\News as Reviews;

class ProductionController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    private function getCategoryList ()
    {
        $catalogCategories = Catalog::cats();

        return array_filter($catalogCategories, function ($item, $index) {
            return ($item->depth === '0');
        }, ARRAY_FILTER_USE_BOTH);
    }

    private function getWorks ($cat)
    {   
        $category = Gallery::cat($cat);

        if ($category) {
            return $category->photos();
        } else {
            return [];
        }
    }

    public function actionIndex()
    {
        $page = Page::get('page-catalog');
        $categoryList = $this->getCategoryList();
        // $cats = Catalog::cats();
        // return $this->redirect('/production/' . current($cats)->slug);
        return $this->render('index', [
            'page' => $page,
            'categoryList' => $categoryList
        ]);
    }

    public function actionCategory($cat, $sub = null)
    {
        $parent = null;
        $curretCat = $cat;
        if ($sub) {
            $curretCat = $sub;
            $parent = Catalog::cat($cat);
        }
        $category = Catalog::cat($curretCat);
        $categories = Catalog::tree();
        $categoryMenu = Catalog::menu($categories, 'catalog', $cat);
        $subCategoriesTemp = Catalog::children($cat);

        $works = $this->getWorks($curretCat);
        $certificates = Certificate::cat('certificate');
        $reviews = Reviews::items(['pagination' => ['pageSize' => 10]]);
        $subCategories = [];


        foreach ($reviews as $key => $value) {
            if ($value->model->video) {
                unset($reviews[$key]);
            }
        }

        foreach ($subCategoriesTemp as $key => $item) {

            if ($item->slug !== $curretCat) {
                $subCategories[] = $item;
            }
        }

        $subMenu = Catalog::menu($subCategories, 'subcatalog', $cat, Catalog::cat($cat));
        // var_dump($reviews); exit;

        Reviews::plugin();
        Gallery::plugin();

        return $this->render('category', [
            'categoryMenu' => $categoryMenu,
            'category' => $category,
            'categoryId' => $sub ?: $cat,
            'subCategories' => $subCategories,
            'parent' => $parent,
            'subMenu' => $subMenu,
            'items' => $category->items(['pagination' => ['pageSize' => 9]]),
            'works' => $works,
            'reviews' => array_slice($reviews, 0, 2),
            'certificates' => $certificates->photos(['pagination' => ['pageSize' => 6]]),
        ]);
    }
}