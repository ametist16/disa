<?php
	use yii\helpers\Url;
	use app\modules\pages\api\Page;
	use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="container">
		<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>

		<div class="section__text section__text_left">
			<?=$page->text;?>
		</div>

	</div>
</section>

<section class="section">
	<div class="container">
		<div class="certificate">
			<div class="certificate__list">
				<? foreach($certificates as $item): ?>
					<div class="certificate__item">
						<a class="easyii-box" href="<?= $item->image; ?>">
							<img src="<?= $item->thumb(210); ?>" alt="">
						</a>
					</div>
				<? endforeach; ?>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">

		<div class="section__text section__text_left">
			<?=$page->text_2;?>
		</div>

	</div>
</section>
