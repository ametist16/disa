<?
	use yii\easyii\helpers\Image;

	$works = [
		'/f/i/works/information_items_258.png',
		'/f/i/works/information_items_288.jpg',
		'/f/i/works/information_items_430.jpg',
		'/f/i/works/information_items_497.jpg',
		'/f/i/works/information_items_528.png',
		'/f/i/works/information_items_539.jpg',
	];
?>

<div class="works-tabs">
	<div class="works-tabs__inner js__tab">

		<div class="works-tabs__tab-head">
			<? foreach($categoryWorks as $i => $item): ?>
				<?php if(count($item['items']) != 0): ?>
					<div class="works-tabs__tab-head-item btn js__tab-btn <?= ($i == 0 ? 'is-active' : '') ?> "><?= $item['label'] ?></div>
				<?php endif; ?>
			<? endforeach; ?>
		</div>
		<div class="works-tabs__tab-list">
			<? foreach($categoryWorks as $i => $item): ?>
				<?php if(count($item['items']) != 0): ?>
				<div class="works-tabs__tab js__tab-item <?= ($i == 0 ? 'is-active' : '') ?>" <?= ($i != 0 ? 'style="display:none;"' : '') ?>>
					<div class="works-slider">
						<div style="display: none;"> <?= var_dump(count($item['items'])) ?></div>
						<div class="works-slider__list">

							<? foreach($item['items'] as $photoItem): ?>
								<div class="easyii-box works-slider__item" href="<?= $photoItem->image; ?>">
									<div class="works-slider__zoom">
										<img src="/f/i/icon-zoom.png" alt="" class="works-slider__zoom-icon">
										<img src="/f/i/icon-zoom-hover.png" alt="" class="works-slider__zoom-icon works-slider__zoom-icon_hover">
									</div>
									<div class="works-slider__img-wrapper">
										<img src="<?= $photoItem->thumb(200, 200); ?>" alt="">
									</div>
									<div class="works-slider__content">
										<div class="works-slider__text">
											<?= $photoItem->description; ?>
										</div>
									</div>
								</div>
							<? endforeach; ?>
						</div>
					</div>
					<div class="works-tabs__btn">
						<a href="/our-works/<?= $item['url'] ?>" class="btn">посмотреть все фотографии</a>
					</div>
				</div>
				<?php endif; ?>
			<? endforeach; ?>


	</div>
</div>