<?php
use yii\widgets\Menu;
use app\modules\works\api\Gallery;
use yii\widgets\Breadcrumbs;
use app\modules\pages\api\Page;

Gallery::plugin();

$this->title = $category->seo('title', $category->model->title);
$this->registerMetaTag(
	['name' => 'description',
	'content' => $category->seo('description', $category->model->title)
]);

$this->params['breadcrumbs'][]  =['label' => 'Наши работы'];

$this->params['breadcrumbs'][] = ['label' => $category->model->title];

?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">

	<div class="container">

		
		<div class="page">
			<div class="page__inner">
				<div class="page__col page__col_left">
					<!-- Меню категорий -->
					<nav>
						<?= Menu::widget($categoryMenu); ?>
					</nav>
				</div>
				<div class="page__col page__col_right">
					<!-- <h1 class="section__title"><?= Page::title($category->seo('h1', $category->title)); ?></h1> -->
					<h1 class="section__title"><?= Page::title('Наши работы'); ?></h1>

					<!-- Элементы категории -->
					<div class="our-works">
						<div class="our-works__list">
							<? foreach($items as $item): ?>
								<a class="easyii-box our-works__item" href="<?= $item->image; ?>">
									<div class="our-works__zoom">
										<img src="/f/i/icon-zoom.png" alt="" class="our-works__zoom-icon">
										<img src="/f/i/icon-zoom-hover.png" alt="" class="our-works__zoom-icon our-works__zoom-icon_hover">
									</div>
									<div class="our-works__img-wrapper">
										<img src="<?= $item->thumb(430, 430); ?>" alt="">
									</div>
									<div class="our-works__content">
										<div class="our-works__text">
											<?= $item->description; ?>
										</div>
									</div>
								</a>
							<? endforeach; ?>
						</div>
					</div>
					
					<!-- Пагинация -->
					<?= $category->pages() ?>
				</div>
			</div>
		</div>
	</div>

</section>