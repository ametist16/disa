<div class="clients-slider">
	<div class="clients-slider__inner">
		<div class="clients-slider__list">
			<div class="clients-slider__item"><img src="/f/i/clients/agroinvest-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/instroy-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/metr-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/morf-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/pishekombinat-bezhickiy-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/bryanskselmash-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/pochta-rossii-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/ptibo-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/rjd-client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/transneft--client-img.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/km.png" alt=""></div>
			<div class="clients-slider__item"><img src="/f/i/clients/vega.jpg" alt=""></div>
		</div>
	</div>
</div>