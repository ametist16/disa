<?php
use yii\easyii\widgets\DateTimePicker;
use yii\easyii\helpers\Image;
use yii\easyii\widgets\TagsInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;

$module = $this->context->module->id;
?>
<?php $form = ActiveForm::begin([
    'enableAjaxValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>
<?= $form->field($model, 'title') ?>

<div class="row">
    <div class="col-sm-6">
        <?php if($model->image) : ?>
            <img src="<?= Image::thumb($model->image, 240) ?>">
            <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->news_id, 'key' => 'image']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
        <?php endif; ?>
        <?= $form->field($model, 'image')->fileInput() ?>
    </div>
    <div class="col-sm-6">
        <?php if($model->image_preview) : ?>
            <img src="<?= Image::thumb($model->image_preview, 240) ?>">
            <a href="<?= Url::to(['/admin/'.$module.'/a/clear-image', 'id' => $model->news_id, 'key' => 'image_preview']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
        <?php endif; ?>
        <?= $form->field($model, 'image_preview')->fileInput() ?>
    </div>
</div>


<?php if($this->context->module->settings['enableShort']) : ?>
    <?= $form->field($model, 'short')->textarea() ?>
<?php endif; ?>
<?= $form->field($model, 'text')->widget(Redactor::className(),[
    'options' => [
        'minHeight' => 400,
        'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'companynews']),
        'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'companynews']),
        'plugins' => ['fullscreen']
    ]
]) ?>

<?= $form->field($model, 'time')->widget(DateTimePicker::className(), [
    'options' => [
        'format' => 'D.MM.YYYY',
    ]
]); ?>

<?php if($this->context->module->settings['enableTags']) : ?>
    <?= $form->field($model, 'tagNames')->widget(TagsInput::className()) ?>
<?php endif; ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>
