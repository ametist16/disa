const oForm = {
	init: function() {
		$(document).on('submit', '.js__form', oForm.mSubmit);
	},
	mSubmit: function(e) {
		e.preventDefault();

		var jForm = $(this),
			jInputs = jForm.find('input, textarea'),
			jResponse = jForm.find('.response'),
			jSubmit = jForm.find('[type="submit"]'),
			bError = 1;

		jInputs.each(function(){
			var jInput = $(this),
				sType = jInput.data('type'),
				sVal = jInput.val();

			oForm.mRegTest(jInput, sType, sVal);
		});

		jInputs.each(function(){
			var jInput = $(this);

			if(jInput.hasClass('is-error')){
				bError = 0;
			}
		})
		// console.log(bError);
		if(bError){

			console.log(jForm.serialize());
			$.ajax({
				url: '/send',
				type: 'GET',
				data: jForm.serialize(),
			}).always(function(){
				jSubmit.attr('disabled', 'disabled');
			}).done(function(response) {
				console.log(response);
				jSubmit.removeAttr('disabled');

				if(jResponse.length >0){
					jResponse.html(response).fadeIn();
				}
				jForm[0].reset();

				if (jForm.data('submit-success')) {
					yaCounter32137425.reachGoal(jForm.data('submit-success'));
				}
			}).fail(function(response){
				console.log(response);
				if(jResponse.length >0){
					jResponse.html(response).fadeIn();
				}
			});
		}
		
		return false;
	},
	mRegTest: function(jInput, sType, sVal) {
		switch (sType) {
			case 'name':
				var rv_name = /^[a-zA-Zа-яА-Я]+$/;
				if (sVal.length >= 2 && sVal != '' && rv_name.test(sVal)) {
					jInput.removeClass('is-error')
				} else {
					jInput.addClass('is-error');
				}
				break;
			case 'phone':
				var rv_tel = /^((8|\+7)[\- ]?)?(\(?\d{3,4}\)?[\- ]?)?[\d\- ]{5,10}$/;
				if (sVal != '' && rv_tel.test(sVal)) {
					jInput.removeClass('is-error')
				} else {
					jInput.addClass('is-error');
				}
				break;
			case 'email':
				var email_reg = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
				if (sVal != '' && email_reg.test(sVal)) {
					jInput.removeClass('is-error')
				} else {
					jInput.addClass('is-error');
				}
				break;
			case 'email-phone':
				var email_reg = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
				var rv_tel = /^((8|\+7)[\- ]?)?(\(?\d{3,4}\)?[\- ]?)?[\d\- ]{5,10}$/;

				if ((sVal != '' && email_reg.test(sVal)) || (sVal != '' && rv_tel.test(sVal))) {
					jInput.removeClass('is-error')
				} else {
					jInput.addClass('is-error');
				}
				break;
			case 'checkbox':
				if(jInput.is(':checked')){
					jInput.removeClass('is-error')
				} else {
					jInput.addClass('is-error');
				}
				break;
			case 'requared':
				if(sVal != ''){
					jInput.removeClass('is-error')
				} else {
					jInput.addClass('is-error');
				}
		}
	}
}

$(function() {
	oForm.init();
});
