<?php
	use app\modules\pages\api\Page;
	$this->title = $page->seo('title', $page->model->title);
	$this->registerMetaTag(
		['name' => 'description',
		'content' => $page->seo('description', $page->model->title)
	]);

	use yii\helpers\Url;
?>

	<?= $this->render('_top-slider', [
		'slides' => $slides
	]);?>

	<section class="section">
		<div class="container">
			<div class="section__title"><?= Page::title('популярные предложения окон'); ?></div>
			<div class="section__text">Мы предлагаем купить окна из  ПВХ и алюминия на выгодных условиях. Стоимость пластиковых окон формируется исходя из выбранного варианта стеклопакета, количества глухих и открывающихся створок, вида профиля, фурнитуры (оконные ручки, петли, ограничители, фиксаторы, замки). Точные расчеты по ценам производят специалисты  индивидуально для каждого клиента после выполнения замеров проемов.</div>
			<?= $this->render('_popular', []);?>
		</div>
	</section>

<div class="section__title">Пройдите тест <br> <span class="section__title_separate section__title_xsmall">и получите полный расчет <br> <span class="section__title_blue">стоимости своих окон, <br> скидку до 20% и подарок</span></span></div>
	
	<div class="marquiz__container marquiz__container_inline">
  <a class="marquiz__button marquiz__button_blicked marquiz__button_rounded marquiz__button_shadow" href="#popup:marquiz_5fc6239e710f3a0044ded0ed" data-fixed-side="" data-alpha-color="rgba(0, 168, 236, 0.5)" data-color="#00A8EC" data-text-color="#ffffff">Пройти тест</a>
</div>
	<br><br>
<!--	<section class="section section_get-measure">
		<div class="container">
			<div class="get-measure">
				<div class="get-measure__inner">
					<div class="get-measure__title">Оставьте заявку на замер</div>
					<div class="get-measure__body">
						<div class="get-measure__content">
							<div class="get-measure__main-img"><img src="/f/i/presents-stuff.png" alt=""></div>
							<div class="get-measure__present">
								<div class="get-measure__present-img"><img src="/f/i/icon-present.png" alt=""></div>
								<div class="get-measure__present-text">И получите комплект <br>по уходу за окнами <br>в подарок!</div>
							</div>
							<div class="get-measure__list">
								<div class="get-measure__list-header">Подарки:</div>
								<div class="get-measure__list-item">моющее средство для пвх окон</div>
								<div class="get-measure__list-item">средство для ухода за уплотнителем </div>
								<div class="get-measure__list-item">смазка для фурнитуры</div>
								<div class="get-measure__list-item">многоразовая салфетка “Микроспан”</div>
							</div>
						</div>
						<div class="get-measure__form-wrapper">
							<form action="" class="get-measure__form js__form">
								<div class="get-measure__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
										</div>
									</div>
								</div>
								<div class="get-measure__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
										</div>
									</div>
								</div>
								<div class="get-measure__checkbox">
									<label class="checkbox">
										<input type="checkbox" class="checkbox__input" data-type="checkbox">
										<div class="checkbox__icon"></div>
										Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
									</label>
								</div>
								<div class="get-measure__btn">
									<button class="btn">Оставьте заявку на замер</button>
								</div>
								<div class="response"></div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section> -->

	<?= $this->render('_catalog', [
		'categoryList' => $categoryList
	]);?>

	<section class="section section_gray section_about">
		<div class="section__inner">
			<div class="container">
				<div class="section__title">
					<?= Page::title('об окнах диса'); ?>
				</div>
				<div class="section__text">
					<p>В 2003 году новым участником рынка производства светопрозрачных конструкций стала компании «Окна ДИСА». Динамично развивающееся предприятие уже более 12 лет изготавливает металлопластиковые окна и двери ПВХ и изделия из алюминиевого профиля, используя традиционные для данных продуктов технологии и богатый профессиональный опыт.</p>
					<p>Собственные производственные мощности компании «Окна ДИСА» позволяют изготавливать широкое разнообразие светопрозрачных конструкций из профиля ПВХ и алюминия высочайшего качества, как типовые, так и эксклюзивные, технологически сложные изделия.</p>
				</div>
				<div class="section__link">
					<a href="/about/about-us">читать всю историю</a>
					
				</div>
			</div>
		</div>
	</section>
	<section class="section section_service-step">
		<div class="section__inner">
		<div class="container">
			<div class="section__title"><?= Page::title('пластиковые окна «ОТ И ДО»'); ?></div>
			<div class="section__subtitle">Возьмем на себя все ваши заботы по установке окон</div>
			<div class="section__text">
				<p>У нас вы можете не только заказать пластиковые или алюминиевые  окна в Брянске по выгодной цене, но и воспользоваться всеми<br> преимуществами профессионального сервиса. Выполнение замеров предоставляется бесплатно. Опытные мастера обеспечат <br>квалифицированное консультирование по любым вопросам, грамотный монтаж.</p>
			</div>
		</div>
		<div class="service-step">
			<div class="service-step__list">
				<? foreach ($services as $key => $service): ?>

					<? if ($key === 2): ?>
						<div class="service-step__item" style="background-image: url('/f/i/2-08325521f.jpg');">
							<div class="service-step__row">
								<div class="service-step__num">3</div>
								<div class="service-step__content">
									<div class="service-step__title">Заключение договора и&nbsp;оплата</div>
									<div class="service-step__btn">
										<a href="#" class="btn">варианты оплаты заказа</a>
									</div>
								</div>
							</div>
						</div>
						<? continue ?>
					<? endif; ?>

					<? if ($key < 6): ?>
						<? $img = $service->model->image_preview ? $service->model->image_preview : $service->image; ?>
						<div class="service-step__item" style="background-image: url('<?= $img; ?>');" >
							<div class="service-step__row">
								<div class="service-step__num"><?=$key + 1;?></div>
								<div class="service-step__content">
									<div class="service-step__title"><?=$service->title;?></div>
									<div class="service-step__btn">
										<? if ($service->slug === 'professional-consultation'): ?>
											<button class="btn" data-popup-open="metering">Заказать консультацию</button>
										<? elseif ($service->slug === 'free-froze'): ?>
											<button class="btn" data-popup-open="metering">Вызвать замерщика</button>
										<? else: ?>
											<a href="<?= Url::toRoute(['services/view', 'slug' => $service->slug]); ?>" class="btn">Подробнее</a>
										<? endif; ?>
									</div>
								</div>
							</div>
						</div>
					<? endif; ?>

				<? endforeach; ?>

			</div>
		</div>
		</div>
	</section>
	<section class="section">
		<div class="section__inner">
			<div class="container">
				<div class="section__title"><?= Page::title('как мы завоевываем доверие'); ?></div>
				<?= $this->render('_advantages', []);?>
			</div>
		</div>
	</section>
	<section class="section section_get-now">
		<div class="container">
			<div class="get-now">
				<div class="get-now__inner">
					<div class="get-now__left">
						<div class="get-now__window"><img src="/f/i/get-now-window.png" alt=""></div>
						<div class="get-now__payment">
							<div class="get-now__payment-title">Другие <br>способы <br>оплаты:</div>
							<div class="get-now__payment-list">
								<div class="get-now__payment-item">Наличный <br>расчёт</div>
								<div class="get-now__payment-item">Безналичный <br>расчёт</div>
								<div class="get-now__payment-item">Банковские <br>карты</div>
								<div class="get-now__payment-item">Оплата в кредит</div>
							</div>
						</div>
					</div>
					<div class="get-now__right">
						<div class="get-now__title">закажите сейчас, платите потом</div>
						<div class="get-now__text"><div class="get-now__text-icon"><img src="/f/i/icon-clock.png" alt=""></div> Оформите рассрочку без первоначального <br>взноса. Первый платеж через месяц!</div>
						<form action="" class="get-now__form-wrapper js__form">
							<div class="get-now__form-row">
								<div class="get-now__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
										</div>
									</div>
								</div>
								<div class="get-now__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
										</div>
									</div>
								</div>
							</div>
							<div class="get-now__checkbox">
								<label class="checkbox">
									<input type="checkbox" class="checkbox__input" data-type="checkbox">
									<div class="checkbox__icon"></div>
									Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
								</label>
							</div>
							<div class="get-now__btn">
								<button class="btn">Получить проект</button>
							</div>
							<div class="response"></div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section">
		<div class="section__inner">
			<div class="container">
				<div class="section__title"><?= Page::title('примеры выполненных работ'); ?></div>
				<?= $this->render('_works-tabs', [
					'works' => $works,
					'categoryWorks' => $categoryWorks
				]);?>
			</div>
		</div>
	</section>
	<section class="section section_gray">
		<div class="section__inner">
			<div class="container">
				<div class="section__title"><?= Page::title('что говорят о нас клиенты'); ?></div>

				<?= $this->render('_feedback-slider', [
					'reviews' => $reviews,
				]);?>
			</div>
		</div>
	</section>
	<section class="section section_get-project">
		<div class="container">
			<div class="get-project">
				<div class="get-project__inner">
					<div class="get-project__body">
						<div class="get-project__content">
							<div class="get-project__title">бесплатно <br>создадим проект <br>для ваших окон</div>
							<div class="get-project__text">Менеджер по снятым замерам в специальной программе cоздает макет изделия со всеми характеристиками. Таким образом, вы получаете несколько вариантов, различных по цене  и комплектации.</div>
						</div>
						<div class="get-project__form-wrapper">
							<form action="" class="get-project__form js__form">
								<div class="get-project__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
										</div>
									</div>
								</div>
								<div class="get-project__input">
									<div class="input">
										<div class="input__wrapper">
											<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
										</div>
									</div>
								</div>
								<div class="get-project__checkbox">
									<label class="checkbox">
										<input type="checkbox" class="checkbox__input" data-type="checkbox">
										<div class="checkbox__icon"></div>
										Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
									</label>
								</div>
								<div class="get-project__btn">
									<button class="btn">Получить проект</button>
								</div>
								<div class="response"></div>
							</form>
						</div>
						<div class="get-project__ruler"><img src="/f/i/ruler.png" alt=""></div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section class="section">
		<div class="section__inner">
			<div class="container">
				<div class="section__title">наши <span class="section__title_blue">клиенты</span></div>
				<?= $this->render('_clients-slider', []);?>
			</div>
		</div>
	</section>
	<section class="section section_gray section_get-callback">
		<div class="section__inner">
			<div class="container">
				<div class="section__title"><?= Page::title('есть вопрос, на который <br>не нашли ответ'); ?></div>
				<div class="section__subtitle">Закажите звонок, и мы Вам перезвоним</div>
				<div class="get-callback">
					<div class="get-callback__inner">
						<form class="get-callback__form js__form">
							<div class="get-callback__input">
								<div class="input">
									<div class="input__wrapper">
										<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
									</div>
								</div>
							</div>
							<div class="get-callback__input">
								<div class="input">
									<div class="input__wrapper">
										<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
									</div>
								</div>
							</div>
							<div class="get-callback__checkbox">
								<label class="checkbox">
									<input type="checkbox" class="checkbox__input" data-type="checkbox">
									<div class="checkbox__icon"></div>
									Я ознакомился и <br>согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
								</label>
							</div>
							<div class="get-callback__btn">
								<button class="btn">Отправить</button>
							</div>
							<div class="response"></div>
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</section>

