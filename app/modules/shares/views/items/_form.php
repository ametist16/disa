<?php
use yii\easyii\helpers\Image;
use yii\easyii\widgets\DateTimePicker;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\easyii\widgets\Redactor;
use yii\easyii\widgets\SeoForm;

$settings = $this->context->module->settings;
$module = $this->context->module->id;
?>

<?php $form = ActiveForm::begin([
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>
<?= $form->field($model, 'title') ?>

<div class="row">
    <div class="col-sm-6">
        <?php if($model->image) : ?>
            <img src="<?= Image::thumb($model->image, 240) ?>">
            <a href="<?= Url::to(['/admin/'.$module.'/items/clear-image', 'id' => $model->primaryKey, 'key' => 'image']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
        <?php endif; ?>
        <?= $form->field($model, 'image')->fileInput() ?>
    </div>
    <div class="col-sm-6">
        <?php if($model->image_preview) : ?>
            <img src="<?= Image::thumb($model->image_preview, 240) ?>">
            <a href="<?= Url::to(['/admin/'.$module.'/items/clear-image', 'id' => $model->primaryKey, 'key' => 'image_preview']) ?>" class="text-danger confirm-delete" title="<?= Yii::t('easyii', 'Clear image')?>"><?= Yii::t('easyii', 'Clear image')?></a>
        <?php endif; ?>
        <?= $form->field($model, 'image_preview')->fileInput() ?>
    </div>
</div>


<?= $dataForm ?>
<?php if($settings['itemDescription']) : ?>
    <?= $form->field($model, 'short')->widget(Redactor::className(),[
        'options' => [
            'minHeight' => 140,
            'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'shares'], true),
            'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'shares'], true),
            'plugins' => ['fullscreen']
        ]
    ]) ?>
    <?= $form->field($model, 'description')->widget(Redactor::className(),[
        'options' => [
            'minHeight' => 400,
            'imageUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'shares'], true),
            'fileUpload' => Url::to(['/admin/redactor/upload', 'dir' => 'shares'], true),
            'plugins' => ['fullscreen']
        ]
    ]) ?>
<?php endif; ?>

<?= $form->field($model, 'constant')->checkbox([
        // 'label' => 'Неактивный чекбокс',
        // 'labelOptions' => [
        //     'style' => 'padding-left:20px;'
        // ],
        // 'disabled' => true
    ]); ?>

<?//= $form->field($model, 'available') ?>
<?//= $form->field($model, 'price') ?>
<?//= $form->field($model, 'discount') ?>

<?= $form->field($model, 'time')->widget(DateTimePicker::className(), [
    'options' => [
        'format' => 'D.MM.YYYY',
    ]
]); ?>

<?php if(IS_ROOT) : ?>
    <?= $form->field($model, 'slug') ?>
    <?= SeoForm::widget(['model' => $model]) ?>
<?php endif; ?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>