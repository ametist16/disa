<?php
namespace app\modules\certificate\controllers;

use yii\easyii\components\CategoryController;
use app\modules\certificate\models\Category;

class AController extends CategoryController
{
    public $categoryClass = 'app\modules\certificate\models\Category';
    public $moduleName = 'certificate';
    public $viewRoute = '/a/photos';

    public function actionPhotos($id)
    {
        if(!($model = Category::findOne($id))){
            return $this->redirect(['/admin/'.$this->module->id]);
        }

        return $this->render('photos', [
            'model' => $model,
        ]);
    }
}