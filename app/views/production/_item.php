<div class="product-item">
	<div class="product-item__inner">
		<div class="product-item__row">
			<div class="product-item__left">
				<div class="product-item__img-wrap">
					<img src="<?= $item->image; ?>" alt="">
				</div>
				<div class="product-item__price"><?= $item->price_str; ?></div>
			</div>
			<div class="product-item__right">
				<div class="product-item__title"><?= $item->title; ?></div>
				<div class="product-item__subtitle"><?= $item->subtitle; ?></div>
				<? if($item->props): ?>
					<div class="product-item__props">
						<? foreach($item->props as $props): ?>
							<div class="product-item__prop">
								<div class="product-item__prop-name"><?= $props->name; ?></div>
								<div class="product-item__prop-value"><?= $props->value; ?></div>
							</div>
						<? endforeach; ?>
					</div>
				<? endif; ?>
				<? if($item->advantages): ?>
					<div class="section__text">
						<ul class="product-item__advantages">
							<? foreach($item->advantages as $advantage): ?>
								<li class="product-item__advantage">
									<?= $advantage->name; ?>
								</li>
							<? endforeach; ?>
						</ul>
					</div>
					
				<? endif; ?>

				<? if($item->model->warranty || $item->model->installment): ?>
					<div class="product-item__bonuses">
						<? if($item->model->warranty): ?>
							<div class="product-item__bonus-warranty">
								<div class="product-item__warranty-icon"></div>
								<div class="product-item__warranty-content">
									<div class="product-item__bonus-title">Гарантии</div>
									<?= $item->model->warranty; ?>
								</div>
							</div>
						<? endif; ?>
						<? if($item->model->installment): ?>
							<div class="product-item__bonus-installment">
								<div class="product-item__installment-icon"></div>
								<?= $item->model->installment; ?>
							</div>
						<? endif; ?>
					</div>
				<? endif; ?>

				<div class="product-item__btn">
					<button class="btn" data-popup-open="calculate" onclick="yaCounter32137425.reachGoal('catalog')">Расчитать стоимость</button>
				</div>
				<span data-popup-open="form" class="product-item__get-consult">
					<div class="product-item__get-consult-icon"><img src="/f/i/icon-get-consult.png" alt=""></div>
					Отправить заявку <br>и получить консультацию
				</span>
			</div>
		</div>

		<? if($item->photos): ?>
			<div class="product-item__row">
				<div class="product-item__description">
					<!-- Дополнительные фото товара -->
					<?= $this->render('_slides', [
						'slides' => $item->photos
					]);?>
				</div>
			</div>
		<? endif; ?>

		<? if($item->description): ?>
		<div class="product-item__row">
			<div class="product-item__description section__text">
				<!-- Текстовое описание товара (его нет в макете но оно необходимо) -->
				<?= $item->description; ?>
			</div>
		</div>
		<? endif; ?>
	</div>
</div>
