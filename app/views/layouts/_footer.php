<?php

use yii\easyii\models\Setting;
use yii\widgets\Menu;
use app\modules\productions\api\Catalog;
use app\modules\services\api\Services;

$categories = Catalog::tree();
$categoryMenu = Catalog::menu($categories, 'footer', $this->context->actionParams['cat']);

use app\modules\shops\api\Shops;

$offices = Shops::all();
?>

<footer class="footer">
	<div class="container">
		<div class="footer__inner">
			<div class="footer__left">
				<a href="/" class="footer__logo"><img src="/f/i/footer-logo.png" alt=""></a>
				<div class="footer__soc">
					<a href="https://ok.ru/oknadisa" target="_blank" class="footer__soc-item footer__soc-item_ok"><img src="/f/i/icon-ok.png" alt=""></a>
					<a href="https://vk.com/oknadisa" target="_blank" class="footer__soc-item footer__soc-item_vk"><img src="/f/i/icon-vk.png" alt=""></a>
					<a href="https://www.facebook.com/oknadisa/" target="_blank" class="footer__soc-item footer__soc-item_fb"><img src="/f/i/icon-fb.png" alt=""></a>
					<a href="https://www.instagram.com/oknadisa/" target="_blank" class="footer__soc-item footer__soc-item_insta"><img src="/f/i/icon-insta.png" alt=""></a>
				</div>
				<button class="footer__btn" data-popup-open="form">Заказать звонок</button>
			</div>
			<div class="footer__menu">
				<nav class="footer-menu">
					<?= Menu::widget($categoryMenu); ?>
				</nav>
			</div>
			<div class="footer__right">
				<div class="footer__office">
					<div class="footer__office-title">Наши офисы:</div>
					<div class="footer__office-list">
						<!-- <? foreach ($offices as $key => $office): ?>
							<div class="footer__office-item">
								<div class="footer__office-area"><?= $office['district'] ? $office['district'] : $office['city'] ;?>: </div>
								<div class="footer__office-location"><?= $office['address']; ?> </div>
								<a href="tel:<?= $office['phone']; ?>" class="footer__office-phone"><?= $office['phone']; ?></a>
							</div>
						<? endforeach; ?> -->


						<div class="footer__office-item">
							<div class="footer__office-area">Бежицкий район: </div>
							<div class="footer__office-location">ул. Харьковская, 2 </div>
							<a href="tel:+74832305757" class="footer__office-phone">+7 (4832) 30-57-57</a>
						</div>
						<!-- 	<div class="footer__office-item">
							<div class="footer__office-area">Володарский район:</div>
							<div class="footer__office-location">ул. 2-я Мичурина, 42а</div>
							<a href="tel:+74832735555" class="footer__office-phone">+7 (4832) 73-55-55</a>
						</div> -->
						<div class="footer__office-item">
							<div class="footer__office-area">Советский район: </div>
							<div class="footer__office-location">ул. Ромашина, 1а</div>
							<a href="tel:+74832642222" class="footer__office-phone">+7 (4832) 64-22-22</a>
						</div>
						<div class="footer__office-item">
							<div class="footer__office-area">г.Севск </div>
							<div class="footer__office-location">ул. Ленина, 5 </div>
							<a href="tel:+74835691311" class="footer__office-phone">+7 (48356) 9-13-11</a>
						</div>
						<div class="footer__office-item">
							<div class="footer__office-area">г. Дятьково</div>
							<div class="footer__office-location">ул. Ленина, 185 </div>
							<a href="tel:+74833332990" class="footer__office-phone">+7 (48333) 3-29-90</a>
						</div>
						<div class="footer__office-item">
							<div class="footer__office-area">п. Локоть</div>
							<div class="footer__office-location">ул. Липовая аллея, 41</div>
							<a href="tel:+74835493373" class="footer__office-phone">+7 (48354) 9-33-73</a>
						</div>



					</div>
				</div>
				<div class="footer__copy">
					<div class="footer__copy-text">Все права защищены 2020 © Производственная компания “Окна Диса”</div>
					<span class="footer__copy-link" data-popup-open="agreement">Политика в отношении обработки персональных данных</span>
				</div>
			</div>
		</div>
		
	</div>
</footer>
