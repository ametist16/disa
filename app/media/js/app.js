"use strict";

var Career = {
  init: function init() {
    $(document).on('click', Career.options.careerBtn, this.onClickBtn);
  },
  options: {
    careerItem: '.js__career',
    careerBtn: '.js__career-btn',
    careerBody: '.js__career-body',
    classActive: 'is-opened'
  },
  onClickBtn: function onClickBtn(event) {
    var btn = $(event.currentTarget);
    Career.toggleBody(btn);
  },
  toggleBody: function toggleBody(btn) {
    var item = btn.closest(Career.options.careerItem);
    var body = item.find(Career.options.careerBody);
    btn.toggleClass(Career.options.classActive);

    if (btn.hasClass(Career.options.classActive)) {
      body.slideDown(300);
    } else {
      body.slideUp(300);
    }
  }
};
$(function () {
  Career.init();
});
"use strict";

var CatalogSpoiler = {
  init: function init() {
    $(document).on('click', CatalogSpoiler.options.catalogBtn, this.onChangeCatalog);

    if ($(CatalogSpoiler.options.catalogBtn).length) {
      $(CatalogSpoiler.options.catalogBtn).each(function (index, item) {
        setTimeout(function () {
          $(item).trigger('click');
        }, 2000);
      });
    }
  },
  options: {
    catalog: '.js__catalog-spoiler',
    catalogItem: '.js__catalog-spoiler-item',
    catalogBtn: '.js__catalog-spoiler-btn',
    classActive: 'is-hidden'
  },
  onChangeCatalog: function onChangeCatalog(event) {
    event.preventDefault();
    var btn = $(event.currentTarget);
    var catalog = btn.closest(CatalogSpoiler.options.catalog);
    CatalogSpoiler.toogleItems(catalog, btn);
  },
  toogleItems: function toogleItems(catalog, btn) {
    var items = catalog.find(CatalogSpoiler.options.catalogItem);

    if (btn.hasClass(CatalogSpoiler.options.classActive)) {
      items.each(function (index, item) {
        var item = $(item);

        if (index > 5) {
          item.slideDown();
        }
      });
      btn.removeClass(CatalogSpoiler.options.classActive);
      btn.text(btn.data('open-text'));
    } else {
      items.each(function (index, item) {
        var item = $(item);

        if (index > 5) {
          item.slideUp();
        }
      });
      btn.addClass(CatalogSpoiler.options.classActive);
      btn.text(btn.data('hide-text'));
    }
  }
};
$(function () {
  CatalogSpoiler.init();
});
"use strict";

var MobileMenu = {
  init: function init() {
    $(document).on('click', MobileMenu.options.menuBtn, this.onClickMenu);

    if ($(window).width() < 960) {
      $(document).on('click', '.main-menu__item_submenu', this.toogleSubmenu);
    }
  },
  options: {
    menuBtn: '.header-top__menu-mob',
    menu: '.main-menu',
    classActive: 'is-active'
  },
  onClickMenu: function onClickMenu(event) {
    var btn = $(event.currentTarget);
    MobileMenu.toggleMenu(btn);
  },
  toggleMenu: function toggleMenu(btn) {
    btn.toggleClass(MobileMenu.options.classActive);
    $(MobileMenu.options.menu).toggleClass(MobileMenu.options.classActive);
  },
  toogleSubmenu: function toogleSubmenu(event) {
    var item = $(event.currentTarget),
        submenu = item.find('.main-menu__submenu');

    if (item.hasClass('is-opened')) {
      item.removeClass('is-opened');
      submenu.stop().slideUp();
    } else {
      item.addClass('is-opened');
      submenu.stop().slideDown();
    }
  }
};
$(function () {
  MobileMenu.init();
});
"use strict";

var oForm = {
  init: function init() {
    $(document).on('submit', '.js__form', oForm.mSubmit);
  },
  mSubmit: function mSubmit(e) {
    e.preventDefault();
    var jForm = $(this),
        jInputs = jForm.find('input, textarea'),
        jResponse = jForm.find('.response'),
        jSubmit = jForm.find('[type="submit"]'),
        bError = 1;
    jInputs.each(function () {
      var jInput = $(this),
          sType = jInput.data('type'),
          sVal = jInput.val();
      oForm.mRegTest(jInput, sType, sVal);
    });
    jInputs.each(function () {
      var jInput = $(this);

      if (jInput.hasClass('is-error')) {
        bError = 0;
      }
    }); // console.log(bError);

    if (bError) {
      console.log(jForm.serialize());
      $.ajax({
        url: '/send',
        type: 'GET',
        data: jForm.serialize()
      }).always(function () {
        jSubmit.attr('disabled', 'disabled');
      }).done(function (response) {
        console.log(response);
        jSubmit.removeAttr('disabled');

        if (jResponse.length > 0) {
          jResponse.html(response).fadeIn();
        }

        jForm[0].reset();

        if (jForm.data('submit-success')) {
          yaCounter32137425.reachGoal(jForm.data('submit-success'));
        }
      }).fail(function (response) {
        console.log(response);

        if (jResponse.length > 0) {
          jResponse.html(response).fadeIn();
        }
      });
    }

    return false;
  },
  mRegTest: function mRegTest(jInput, sType, sVal) {
    switch (sType) {
      case 'name':
        var rv_name = /^[a-zA-Zа-яА-Я]+$/;

        if (sVal.length >= 2 && sVal != '' && rv_name.test(sVal)) {
          jInput.removeClass('is-error');
        } else {
          jInput.addClass('is-error');
        }

        break;

      case 'phone':
        var rv_tel = /^((8|\+7)[\- ]?)?(\(?\d{3,4}\)?[\- ]?)?[\d\- ]{5,10}$/;

        if (sVal != '' && rv_tel.test(sVal)) {
          jInput.removeClass('is-error');
        } else {
          jInput.addClass('is-error');
        }

        break;

      case 'email':
        var email_reg = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;

        if (sVal != '' && email_reg.test(sVal)) {
          jInput.removeClass('is-error');
        } else {
          jInput.addClass('is-error');
        }

        break;

      case 'email-phone':
        var email_reg = /^([a-z0-9_-]+\.)*[a-z0-9_-]+@[a-z0-9_-]+(\.[a-z0-9_-]+)*\.[a-z]{2,6}$/;
        var rv_tel = /^((8|\+7)[\- ]?)?(\(?\d{3,4}\)?[\- ]?)?[\d\- ]{5,10}$/;

        if (sVal != '' && email_reg.test(sVal) || sVal != '' && rv_tel.test(sVal)) {
          jInput.removeClass('is-error');
        } else {
          jInput.addClass('is-error');
        }

        break;

      case 'checkbox':
        if (jInput.is(':checked')) {
          jInput.removeClass('is-error');
        } else {
          jInput.addClass('is-error');
        }

        break;

      case 'requared':
        if (sVal != '') {
          jInput.removeClass('is-error');
        } else {
          jInput.addClass('is-error');
        }

    }
  }
};
$(function () {
  oForm.init();
});
"use strict";

$(function () {
  var oPopup = {
    init: function init() {
      $(document).on('click', '[data-popup-close]', oPopup.closeSelector);
      $(document).on('click', '[data-popup-open]', oPopup.openSelector);
      $(window).on('resize', oPopup.checkHeight);
    },
    openSelector: function openSelector(event) {
      var jThis = $(event.currentTarget);
      var id = jThis.attr('data-popup-open');
      event.preventDefault();
      console.log(id);
      oPopup.open(id);
    },
    open: function open(id) {
      var jPopUp = $('[data-popup-id="' + id + '"]');
      jPopUp.stop().fadeIn(200).css('z-index', 550).addClass('is-visible');
      $('body').addClass('is-overflow');
      oPopup.checkHeight();
      return false;
    },
    closeSelector: function closeSelector(event) {
      var closer = $(event.currentTarget);
      var id = closer.attr('data-popup-close');
      event.preventDefault();
      oPopup.close(id);
    },
    close: function close(id) {
      var jPopUp = $('[data-popup-id="' + id + '"]');
      jPopUp.stop().fadeOut(200).removeClass('is-visible');
      $('body').removeClass('is-overflow');
      return false;
    },
    checkHeight: function checkHeight() {
      $('.popup:visible').each(function () {
        var jThis = $(this);

        if (jThis.outerHeight() >= $(window).height()) {
          jThis.addClass('is-large');
        } else {
          jThis.removeClass('is-large');
        }
      });
    }
  };
  window.oPopup = oPopup;
  oPopup.init();
});
"use strict";

; // Инициализация Карты

$(function () {
  var oYMap = {
    init: function init() {
      if ($('.js__map').length) {
        $('.js__map').each(function (index, element) {
          ymaps.ready(function () {
            oYMap.initMap(element, index);
          });
        });
      }
    },
    initMap: function initMap(element, index) {
      var jMap = $(element),
          aCenter = jMap.data('map-center') ? jMap.data('map-center') : [0, 0],
          aMark = jMap.data('map-marker') ? jMap.data('map-marker') : [0, 0],
          zoom = jMap.data('map-zoom') ? jMap.data('map-zoom') : 15;
      jMap.attr('id', 'contacts-map-' + index);
      oYMap.yMap = new ymaps.Map('contacts-map-' + index, {
        center: aCenter,
        zoom: zoom
      }, {});
      var placemark = new ymaps.Placemark(aMark, {// hintContent: '',
        // balloonContent: aMark[i][2] || ''
      }, {// iconLayout: 'default#image',
        // iconImageHref: '/uploads/i/marker.png',
        // iconImageSize: [71, 94],
        // iconImageOffset: [-31, -67]
      }); // Добавим наши метки на карту

      oYMap.yMap.geoObjects.add(placemark); //Выключим зумм скроллом
      // oYMap.yMap.behaviors.disable('scrollZoom');
    }
  };
  oYMap.init();
});
"use strict";

$.CreateModule({
  name: 'Sliders',
  data: {},
  options: {
    sliderOptions: {}
  },
  hooks: {
    beforeCreate: function beforeCreate() {},
    create: function create() {
      this._create();
    },
    bindEvent: function bindEvent() {
      $(document).on('changetab', this._reinit.bind(this));
    },
    afterCreate: function afterCreate() {}
  },
  privateMethods: {
    _create: function _create() {
      this._initSlider();
    },
    _initSlider: function _initSlider() {
      if ($(this.element).is(':visible')) {
        $(this.element).slick(this.options.sliderOptions);
      }
    },
    _reinit: function _reinit() {
      // $(this.element).slick('reinit');
      if (!$(this.element).hasClass('slick-initialized')) {
        this._initSlider();
      }
    },
    _unBindEvent: function _unBindEvent() {
      $(this.element).off(this.hash);
    }
  },
  publicMethods: {}
});
$(function () {
  $('.top-slider__slider').sliders({
    options: {
      sliderOptions: {
        arrows: true,
        dots: true,
        responsive: [{
          breakpoint: 768,
          settings: {
            arrows: false
          }
        }]
      }
    }
  });
  $('.js__catalog-certificate').sliders({
    options: {
      sliderOptions: {
        arrows: false,
        dots: true,
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }
  });
  $('.works-slider__list').sliders({
    options: {
      sliderOptions: {
        arrows: true,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        responsive: [{
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: false,
            variableWidth: false,
            dots: true
          }
        }]
      }
    }
  });
  $('.feedback-slider__list').sliders({
    options: {
      sliderOptions: {
        arrows: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        adaptiveHeight: true,
        responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 1,
            centerMode: true,
            arrows: false,
            dots: true
          }
        }]
      }
    }
  });
  $('.clients-slider__list').sliders({
    options: {
      sliderOptions: {
        arrows: true,
        dots: false,
        slidesToShow: 5,
        slidesToScroll: 1,
        rows: 2,
        slidesPerRow: 1,
        responsive: [{
          breakpoint: 1280,
          settings: {
            arrows: false,
            dots: true
          }
        }, {
          breakpoint: 768,
          settings: {
            slidesToShow: 2
          }
        }]
      }
    }
  });
  $('.team-slider__slider').sliders({
    options: {
      sliderOptions: {
        arrows: true,
        dots: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        variableWidth: true
      }
    }
  });
  $('.production-slider__list').sliders({
    options: {
      sliderOptions: {
        arrows: true,
        dots: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [{
          breakpoint: 768,
          settings: {
            arrows: false,
            dots: true
          }
        }]
      }
    }
  });
});
"use strict";

var Tab = {
  init: function init() {
    $(document).on('click', Tab.options.tabBtn, this.onChangeTab);
  },
  options: {
    tab: '.js__tab',
    tabItem: '.js__tab-item',
    tabBtn: '.js__tab-btn',
    classActive: 'is-active'
  },
  onChangeTab: function onChangeTab(event) {
    var btn = $(event.currentTarget);
    var index = btn.index();
    var tabs = btn.closest(Tab.options.tab);
    Tab.setTab(tabs, index);
    $(document).trigger('changetab');
  },
  setTab: function setTab(tabs, index) {
    var allTabs = tabs.find(Tab.options.tabItem);
    var newTab = allTabs.eq(index);
    var allBtn = tabs.find(Tab.options.tabBtn);
    var newBtn = allBtn.eq(index);
    allBtn.removeClass(Tab.options.classActive);
    newBtn.addClass(Tab.options.classActive);
    allTabs.removeClass(Tab.options.classActive).hide();
    newTab.addClass(Tab.options.classActive).show();
  }
};
$(function () {
  Tab.init();
});
"use strict";

var ToTop = {
  init: function init() {
    $(document).on('click', ToTop.options.btn, this.onClick);
    $(document).on('scroll', this.onScroll);
  },
  options: {
    btn: '.btn-to-top'
  },
  onClick: function onClick(event) {
    $('body, html').animate({
      scrollTop: 0
    }, 1500);
  },
  onScroll: function onScroll() {
    var position = $(document).scrollTop();

    if (position > $(window).height()) {
      $(ToTop.options.btn).addClass('is-visible');
    } else {
      $(ToTop.options.btn).removeClass('is-visible');
    }
  }
};
$(function () {
  ToTop.init();
});