<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\pages\api\Page;
use app\modules\shops\api\Shops;

class WhereToBuyController extends Controller
{
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex()
	{
		$page = Page::get('page-where-to-buy');
		$shops = Shops::all();

		return $this->render('index', [
			'page' => $page,
			'shops' => $shops
		]);
	}
}