<?php
namespace app\modules\shops\models;

use Yii;
use yii\easyii\behaviors\CacheFlush;

class Text extends \yii\easyii\components\ActiveRecord
{
    const CACHE_KEY = 'easyii_text_shops';

    public static function tableName()
    {
        return 'app_shopss';
    }

    public function rules()
    {
        return [
            ['text_id', 'number', 'integerOnly' => true],
            [['text', 'address', 'city', 'district', 'coordinates', 'centermap', 'phone', 'worktime', 'manager'], 'trim'],
            ['slug', 'match', 'pattern' => self::$SLUG_PATTERN, 'message' => Yii::t('easyii', 'Slug can contain only 0-9, a-z and "-" characters (max: 128).')],
            ['slug', 'default', 'value' => null],
            ['slug', 'unique']
        ];
    }

    public function attributeLabels()
    {
        return [
            'text' => Yii::t('easyii/shops', 'Text'),
            'city' => 'Город',
            'district' => 'Район',
            'address' => Yii::t('easyii/shops', 'Address'),
            'coordinates' => Yii::t('easyii/shops', 'Coordinates'),
            'centermap' => Yii::t('easyii/shops', 'Centermap'),
            'phone' => Yii::t('easyii/shops', 'Phone'),
            'worktime' => Yii::t('easyii/shops', 'Worktime'),
            'manager' => Yii::t('easyii/shops', 'Manager'),
            'slug' => Yii::t('easyii/shops', 'Slug'),
        ];
    }

    public function behaviors()
    {
        return [
            CacheFlush::className()
        ];
    }
}