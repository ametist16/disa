<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="section__inner">
		<div class="container">
			<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>

			<div class="section__text section__text_left">
				<!-- Текстовый блок верхний -->
				<?=$page->text;?>
				<br>

				<div class="section__text section__text_accent">Мы не просто изготавливаем конструкции из пвх и алюминия, мы предлагаем:</div>
				<!-- Верстка иконок с текстом -->
				<div class="advantages-row">
					<div class="advantages-row__list">
						<div class="advantages-row__item">
							<div class="advantages-row__img-wrap"><img src="/f/i/about/consult.png" alt="" class="advantages-row__img"></div>
							<div class="advantages-row__title">Профессиональную консультацию <br>и подбор-расчет <br>проекта</div>
						</div>
						<div class="advantages-row__item">
							<div class="advantages-row__img-wrap"><img src="/f/i/about/ruler.png" alt="" class="advantages-row__img"></div>
							<div class="advantages-row__title">Бесплатный замер <br>(в городе, области и <br>в других регионах РФ)</div>
						</div>
						<div class="advantages-row__item">
							<div class="advantages-row__img-wrap"><img src="/f/i/about/montage.png" alt="" class="advantages-row__img"></div>
							<div class="advantages-row__title">Профессиональные <br>монтажные работы</div>
						</div>
						<div class="advantages-row__item">
							<div class="advantages-row__img-wrap"><img src="/f/i/about/waranty.png" alt="" class="advantages-row__img"></div>
							<div class="advantages-row__title">Гарантийное <br>обслуживание</div>
						</div>
						<div class="advantages-row__item">
							<div class="advantages-row__img-wrap"><img src="/f/i/about/service.png" alt="" class="advantages-row__img"></div>
							<div class="advantages-row__title">Cервисное <br>сопровождение после <br>окончания гарантии</div>
						</div>
					</div>
				</div>


				<!-- Текстовый блок нижний -->
				<?=$page->text_2;?>
				<div class="section__text section__text_accent">ПОЛНУЮ И ДОСТОВЕРНУЮ ИНФОРМАЦИЮ О ПРЕДОСТАВЛЯЕМЫХ УСЛУГАХ И РЕАЛИЗУЕМОЙ ПРОДУКЦИИ МОЖНО ПОЛУЧИТЬ ПО ТЕЛЕФОНУ +7 (4832) 63-11-99</div>
				
			</div>

		</div>
	</div>
</section>
