<?php
use yii\widgets\Menu;
?>

<nav class="tags-menu">
	<?= Menu::widget([
		'options' => [
			'class' => 'tags-menu__list',
		],
		'items' => [
			['label' => 'Дилерство', 'url' => ['/cooperation/dealership']],
			['label' => 'Корпоративным клиентам', 'url' => ['/cooperation/corporate-client']]
		],
		'itemOptions'=>['class'=>'tags-menu__item'],
		'activeCssClass'=>'is-active',
	]); ?>
</nav>