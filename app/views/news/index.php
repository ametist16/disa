<?php
use yii\helpers\Url;
use yii\widgets\Menu;
use app\modules\pages\api\Page;
use app\modules\works\api\Gallery;
use yii\easyii\helpers\Image;
use yii\widgets\Breadcrumbs;
use app\modules\companynews\api\News;

$this->title = $page->seo('title', $page->model->title);
$this->registerMetaTag(
	['name' => 'description',
	'content' => $page->seo('description', $page->model->title)
]);

$this->params['breadcrumbs'][] = ['label' => 'Новости'];
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="section__inner">
		<div class="container">
			<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>

			<!-- Элементы -->
			<? if ($news && count($news) > 0): ?>

				<div class="stock-list">
					<div class="stock-list__wrapper">
						<div class="stock-list__list">
							<? foreach($news as $item): ?>
								<? $img = $item->model->image_preview ? $item->model->image_preview : $item->image; ?>
								<a href="<?= Url::toRoute(['news/view', 'slug' => $item->slug]); ?>" class="stock-list__item">
									<div class="stock-list__date">
										<div class="stock-list__date-text"><?= $item->date; ?></div>
									</div>
									<div class="stock-list__img">
										<img src="<?= Image::thumb($img, 533, 284, true); ?>" alt="">
									</div>
									<div class="stock-list__title"><?= $item->title; ?></div>
									<div class="stock-list__text"><?= $item->short; ?></div>
								</a>
							<? endforeach; ?>
						</div>
					</div>
				</div>

			<? endif; ?>

			<!-- Пагинация -->
			<?= News::pages() ?>

		</div>
	</div>
</section>
