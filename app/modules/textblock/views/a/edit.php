<?php
$this->title = Yii::t('easyii/textblock', 'Edit text');
?>
<?= $this->render('_menu') ?>
<?= $this->render('_form', ['model' => $model]) ?>