var ToTop = {
	init: function () {
		$(document).on('click', ToTop.options.btn, this.onClick);
		$(document).on('scroll', this.onScroll);
	},
	options: {
		btn: '.btn-to-top'
	},
	onClick (event) {
		$('body, html').animate({scrollTop: 0}, 1500);
	},
	onScroll () {
		var position = $(document).scrollTop();

		if (position > $(window).height()) {
			$(ToTop.options.btn).addClass('is-visible');
		} else {
			$(ToTop.options.btn).removeClass('is-visible');
		}
	}
};

$(function () {
	ToTop.init();
});