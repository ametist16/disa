<?php
	use app\modules\pages\api\Page;
	use yii\helpers\Url;
	use yii\widgets\Breadcrumbs;

	$this->title = $page->seo('title', $page->model->title);
	$this->registerMetaTag(
		['name' => 'description',
		'content' => $page->seo('description', $page->model->title)
	]);

	$this->params['breadcrumbs'][] = ['label' => 'Услуги'];
?>
<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="container">
		<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>

		<div class="section__text section__text_left">
			<?=$page->text;?>
		</div>

	</div>
</section>

<section class="section">

		<div class="service-step">
			<div class="service-step__list">
				<? foreach ($services as $key => $service): ?>
					<? $img = $service->model->image_preview ? $service->model->image_preview : $service->image; ?>
					<div class="service-step__item" style="background-image: url('<?= $img; ?>');" >
						<div class="service-step__row">
							<div class="service-step__num"><?=$key + 1;?></div>
							<div class="service-step__content">
								<div class="service-step__title"><?=$service->title;?></div>
								<div class="service-step__btn">
									<a href="<?= Url::toRoute(['services/view', 'slug' => $service->slug]); ?>" class="btn">заказать</a>
								</div>
							</div>
						</div>
					</div>
				<? endforeach; ?>
			</div>
		</div>

</section>

<section class="section">
	<div class="container">

		<div class="section__text section__text_left">
			<?=$page->text_2;?>
		</div>

	</div>
</section>
