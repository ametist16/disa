<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<div class="section">
		
		<div class="container">
			<div class="stock-item">
				<div class="stock-item__inner">
					<h1 class="stock-item__title"><?= Page::title($item->seo('h1', $item->title)); ?></h1>
					<div class="stock-item__img-wrap">
						<img src="<?= $item->image; ?>" alt="">
						<div class="stock-item__date">
							<div class="stock-item__date-text"><?= $item->getEndDate(); ?></div>
						</div>
					</div>
					<div class="stock-item__text section__text"><?= $item->description; ?></div>
				</div>
			</div>
		</div>
</div>

<div class="section section_gray section_get-stock">
	<div class="section__inner">
		<div class="container">
			<div class="section__title">
				хотите принять участие в акции? <span class="section__title_blue">мы на связи</span></div>
			<div class="section__text">Если у Вас остались вопросы, то свжитесь снами по телефону или заполните форму и напишите свой вопрос ниже.</div>
			<form action="" class="get-stock__form js__form">
				<div class="get-stock__row">
					<div class="get-stock__input">
						<div class="input">
							<div class="input__wrapper">
								<input type="text" class="input__input" placeholder="Ваше имя" data-type="name" name="name">
							</div>
						</div>
					</div>
					<div class="get-stock__input">
						<div class="input">
							<div class="input__wrapper">
								<input type="text" class="input__input" placeholder="Ваш телефон" data-type="phone" name="phone">
							</div>
						</div>
					</div>
				</div>
				<div class="get-stock__row">
					<div class="get-stock__textarea">
						<div class="textarea">
							<div class="textarea__wrapper">
								<textarea name="message" id="" placeholder="Сообщение" class="textarea__textarea" data-type="requared"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="get-stock__row">
					<div class="get-stock__checkbox">
						<label class="checkbox">
							<input type="checkbox" class="checkbox__input" data-type="checkbox">
							<div class="checkbox__icon"></div>
							Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
						</label>
					</div>
					<div class="get-stock__btn">
						<button class="btn">Отправить</button>
					</div>
				</div>
				<div class="response"></div>
			</form>
		</div>
	</div>
</div>


