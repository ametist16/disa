<?php

namespace app\controllers;

use yii\web\Controller;
use app\modules\pages\api\Page;
use app\modules\services\api\Services;

class ServicesController extends Controller
{
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        $page = Page::get('page-services');
        $services = Services::items();

        return $this->render('index', [
            'page' => $page,
            'services' => $services
        ]);
    }

    public function actionView($slug)
    {
        $item = Services::get($slug);

        return $this->render('view', [
            'item' => $item
        ]);
    }
}