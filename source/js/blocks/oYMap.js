;// Инициализация Карты
$(function(){
	var oYMap = {
		init: function(){
			if($('.js__map').length){
				$('.js__map').each(function (index, element) {
					ymaps.ready(function() {
						oYMap.initMap(element, index)
					});
				})
			}
		},
		initMap: function(element, index){
			var jMap = $(element),
				aCenter = jMap.data('map-center') ? jMap.data('map-center') : [0,0],
				aMark = jMap.data('map-marker') ? jMap.data('map-marker') : [0,0],
				zoom = jMap.data('map-zoom') ? jMap.data('map-zoom') : 15;

			jMap.attr('id', 'contacts-map-' + index)

			oYMap.yMap = new ymaps.Map('contacts-map-' + index, {
				center: aCenter,
				zoom: zoom
			}, {});

			var placemark = new ymaps.Placemark(aMark, {
					// hintContent: '',
					// balloonContent: aMark[i][2] || ''
				}, {
					// iconLayout: 'default#image',
					// iconImageHref: '/uploads/i/marker.png',
					// iconImageSize: [71, 94],
					// iconImageOffset: [-31, -67]
				});

			// Добавим наши метки на карту
			oYMap.yMap.geoObjects.add(placemark);

			//Выключим зумм скроллом
			// oYMap.yMap.behaviors.disable('scrollZoom');
		}
	}

	oYMap.init();
});