
<div class="production-slider">
	<div class="production-slider__list">
		<? foreach($slides as $item): ?>
			<div class="production-slider__item">
				<div class="production-slider__zoom">
					<img src="/f/i/icon-zoom.png" alt="" class="production-slider__zoom-icon">
					<img src="/f/i/icon-zoom-hover.png" alt="" class="production-slider__zoom-icon production-slider__zoom-icon_hover">
				</div>
				<?= $item->box(200, 200); ?>
			</div>
		<? endforeach; ?>
	</div>
</div>