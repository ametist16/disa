<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin([
    'enableClientValidation' => true,
    'options' => ['enctype' => 'multipart/form-data', 'class' => 'model-form']
]); ?>
<?php if($model->image) : ?>
    <img src="<?= $model->image ?>" style="width: 848px">
<?php endif; ?>
<?= $form->field($model, 'image')->fileInput() ?>
<?= $form->field($model, 'link') ?>

<?php if($this->context->module->settings['enableTitle']) : ?>
    <?= $form->field($model, 'title')->label('Текст на кнопке') ?>
<?php endif; ?>
<?php if($this->context->module->settings['enableText']) : ?>
    <?= $form->field($model, 'text')->textarea() ?>
<?php endif; ?>

<?
	$model->btn_position = $model->btn_position ? $model->btn_position : 'center';
?>
<?= $form->field($model, 'btn_position')->radioList([
		'left' => 'Слева',
		'center' => 'Центр',
		'right' => 'Справа'
	])->label('Положение кнопки');
?>

<?= Html::submitButton(Yii::t('easyii', 'Save'), ['class' => 'btn btn-primary']) ?>
<?php ActiveForm::end(); ?>