<?php
	use app\modules\textblock\api\Textblock;
?>

<!-- POPUP CALLBACK FORM -->
<div class="popup__overlay" data-popup-id="form">
	<div class="popup popup_form">
		<div class="popup__close" data-popup-close="form"></div>
		<div class="popup__inner">
			<div class="popup__title">отправьте заявку <br><span class="popup__title_blue">и мы перезвоним</span></div>
			<form class="popup__form js__form" data-submit-success="otpravit_call">
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="Ваше имя*" data-type="name" name="name">
						</div>
					</div>
				</div>
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="E-mail" name="email">
						</div>
					</div>
				</div>
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="Телефон*" data-type="phone" name="phone">
						</div>
					</div>
				</div>
				<div class="popup__textarea">
					<div class="textarea">
						<div class="textarea__wrapper">
							<textarea name="message" id="" placeholder="Комментарий:" class="textarea__textarea"></textarea>
						</div>
					</div>
				</div>
				<div class="popup__checkbox">
					<label class="checkbox">
						<input type="checkbox" class="checkbox__input" data-type="checkbox">
						<div class="checkbox__icon"></div>
						Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
					</label>
				</div>
				<div class="popup__btn"><button class="btn">отправить</button></div>
				<div class="response"></div>
			</form>

		</div>
	</div>
</div>

<!-- POPUP CALLBACK FORM -->
<div class="popup__overlay" data-popup-id="metering">
	<div class="popup popup_form">
		<div class="popup__close" data-popup-close="metering"></div>
		<div class="popup__inner">
			<div class="popup__title">закажите бесплатный <br><span class="popup__title_blue">замер</span></div>
			<form class="popup__form js__form" data-submit-success="otpravit_zamer">
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="Ваше имя*" data-type="name" name="name">
						</div>
					</div>
				</div>
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="E-mail" name="email">
						</div>
					</div>
				</div>
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="Телефон*" data-type="phone" name="phone">
						</div>
					</div>
				</div>
				<div class="popup__textarea">
					<div class="textarea">
						<div class="textarea__wrapper">
							<textarea name="message" id="" placeholder="Ваш вопрос" class="textarea__textarea"></textarea>
						</div>
					</div>
				</div>
				<div class="popup__checkbox">
					<label class="checkbox">
						<input type="checkbox" class="checkbox__input" data-type="checkbox">
						<div class="checkbox__icon"></div>
						Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
					</label>
				</div>
				<div class="popup__btn"><button class="btn">заказать бесплатный замер</button></div>
				<div class="response"></div>
			</form>
		</div>
	</div>
</div>

<!-- POPUP PRODUCT -->
<div class="popup__overlay" data-popup-id="calculate">
	<div class="popup popup_form">
		<div class="popup__close" data-popup-close="calculate"></div>
		<div class="popup__inner">
			<div class="popup__title">Рассчитать <span class="popup__title_blue">стоимость</span></div>
			<form class="popup__form js__form" data-submit-success="catalog_otpravit">
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="Ваше имя*" data-type="name" name="name">
						</div>
					</div>
				</div>
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="E-mail" name="email">
						</div>
					</div>
				</div>
				<div class="popup__input">
					<div class="input">
						<div class="input__wrapper">
							<input type="text" class="input__input" placeholder="Телефон*" data-type="phone" name="phone">
						</div>
					</div>
				</div>
				<div class="popup__textarea">
					<div class="textarea">
						<div class="textarea__wrapper">
							<textarea name="message" id="" placeholder="Ваш вопрос" class="textarea__textarea"></textarea>
						</div>
					</div>
				</div>
				<div class="popup__checkbox">
					<label class="checkbox">
						<input type="checkbox" class="checkbox__input" data-type="checkbox">
						<div class="checkbox__icon"></div>
						Я ознакомился и согласен с&nbsp;<span data-popup-open="agreement">правилами</span>
					</label>
				</div>
				<div class="popup__btn"><button class="btn">рассчитать стоимость</button></div>
				<div class="response"></div>
			</form>
		</div>
	</div>
</div>
<!-- POPUP AGREEMENT -->
<div class="popup__overlay" data-popup-id="agreement">
	<div class="popup">
		<div class="popup__close" data-popup-close="agreement"></div>
		<div class="popup__inner">
			<?=textblock::get('agreement');?>
		</div>
	</div>
</div>
