<?php
	use app\modules\pages\api\Page;
	use yii\widgets\Menu;
	use yii\easyii\helpers\Image;
	use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="section__inner">
		<div class="container">
			<h1 class="stock-item__title"><?= Page::title($category->seo('h1', $category->title)); ?></h1>

			<div class="section__text">
				Мы любим радовать своих клиентов, поэтому постоянно устраиваем акции и дарим подарки. В данном разделе вы можете следить за актуальными скидками и выгодными предложениями.
			</div>

			<!-- Меню категорий -->
			<nav class="tags-menu">
				<?= Menu::widget($categoryMenu); ?>
			</nav>

			<!-- Элементы категории -->
			<? if ($items && count($items) > 0): ?>

				<div class="stock-list">
					<div class="stock-list__wrapper">
						<div class="stock-list__list">
							
							<? foreach($items as $item): ?>
								<? $img = $item->model->image_preview ? $item->model->image_preview : $item->image; ?>

								<a href="/promotion/<?= $category->slug; ?>/<?= $item->slug; ?>" class="stock-list__item">
									<div class="stock-list__date">
										<div class="stock-list__date-text"><?= $item->getEndDate(); ?></div>
									</div>
									<div class="stock-list__img">
										<img src="<?= Image::thumb($img, 533, 284, true); ?>" alt="">
									</div>
									<div class="stock-list__title"><?= $item->title; ?></div>
									<div class="stock-list__text"><?= $item->short; ?></div>
								</a>
							<? endforeach; ?>
						</div>
					</div>
				</div>

			<? endif; ?>

			<!-- Пагинация -->
			<?= $category->pages() ?>

		</div>
	</div>
</section>