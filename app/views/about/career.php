<?php
	use yii\helpers\Url;
	use app\modules\pages\api\Page;
	use yii\widgets\Breadcrumbs;
?>

<div class="breadcrumb__wrapper">
	<div class="container">
		<?= Breadcrumbs::widget([
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		])?>
	</div>
</div>

<section class="section">
	<div class="container">
		<h1 class="section__title"><?= Page::title($page->seo('h1', $page->title)); ?></h1>

		<div class="section__text section__text_left">
			<?=$page->text;?>
		</div>
		<div class="career">
			<div class="career__list">
				<div class="career__item js__career">
					<div class="career__head js__career-btn">Грузчик</div>
					<div class="career__body js__career-body">
						<div class="career__row">
							<div class="career__title">Требования:</div>
							<div class="career__text">ответственный, исполнительный, выносливый</div>
						</div>
						<div class="career__row">
							<div class="career__title">Обязанности:</div>
							<div class="career__text">погрузка, разгрузка изделий ПВХ и алюминия, ПВХ-профиля и алюминиевого профиля</div>
						</div>
						<div class="career__row">
							<div class="career__title">Условия:</div>
							<div class="career__text">Оформление по ТК (соц.пакет,оплата отпуска и больничных)<br>
								Оклад + премия<br>
								График работы 6/2 (суббота до 14.00)<br>
								Молодой дружный коллектив
							</div>
						</div>
					</div>
				</div>
				<div class="career__item js__career">
					<div class="career__head js__career-btn">СПЕЦИАЛИСТ ПО ПРОДАЖАМ ИЗДЕЛИЙ ИЗ ПВХ</div>
					<div class="career__body js__career-body">
						<div class="career__row">
							<div class="career__title">Требования:</div>
							<div class="career__text">Опыт в продажах изделий из ПВХ от 1 года <br>
Знание программы «Супер Окна» <br>
Ответственность <br>
Коммуникабельность <br>
Желание работать и зарабатывать</div>
						</div>
						<div class="career__row">
							<div class="career__title">Обязанности:</div>
							<div class="career__text">Консультация клиентов в офисе продаж, <br>
Подбор, расчет и продажа светопрозрачных конструкций <br>
Работа в программе «Супер Окна», работа с отчётностью, документами.</div>
						</div>
						<div class="career__row">
							<div class="career__title">Условия:</div>
							<div class="career__text">Оформление по ТК (соц.пакет,оплата отпуска и больничных) <br>
Оклад + ежемесячные бонусы; <br>
График работы 5/2 (выходные плавающие) <br>
Молодой дружный коллектив</div>
						</div>
					</div>
				</div>
				<div class="career__item js__career">
					<div class="career__head js__career-btn">СЛЕСАРЬ СБОРЩИК АЛЮМИНИЕВЫХ КОНСТРУКЦИЙ</div>
					<div class="career__body js__career-body">
						<div class="career__row">
							<div class="career__title">Требования:</div>
							<div class="career__text">Технически грамотный специалист, ответственный, исполнительный, желание работать и зарабатывать, соблюдение трудовой дисциплины.</div>
						</div>
						<div class="career__row">
							<div class="career__title">Обязанности:</div>
							<div class="career__text">Сборка изделий из алюминия</div>
						</div>
						<div class="career__row">
							<div class="career__title">Условия:</div>
							<div class="career__text">Оформление по ТК (соц.пакет,оплата отпуска и больничных) <br>
Зарплата сдельно-премиальная; <br>
График работы 5/1 (суббота до 14.00) <br>
Молодой дружный коллектив</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section">
	<div class="container">

		<div class="section__text section__text_left">
			<?=$page->text_2;?>
		</div>

	</div>
</section>
